<?php

namespace App\Bundles;

use Tatter\Assets\Bundle;

class BootstrapBundle extends Bundle
{
    protected $paths = [
        'adminlte3/plugins/toastr/toastr.css',
    ];
}
