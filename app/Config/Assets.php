<?php

namespace Config;
/***
 *
 * This file contains example values to override or augment default library behavior.
 * Recommended usage:
 *    1. Copy the file to app/Config/Assets.php
 *    2. Set any override variables
 *    3. Add additional route-specific assets to $routes
 *    4. Remove any lines to fallback to defaults
 *
 ***/

use Tatter\Assets\Config\Assets as AssetsConfig;

class Assets extends AssetsConfig
{
    //--------------------------------------------------------------------
    // Library Behavior
    //--------------------------------------------------------------------

    /**
     * Asset URI base, relative to baseURL.
     *
     * @var string
     */
    public $uri = 'assets/';

    /**
     * Asset storage location in the filesystem.
     * Must be somewhere web accessible.
     *
     * @var string
     */
    public $directory = FCPATH . 'assets/';

    /**
     * Path for third-party published Assets. The path is relative to
     * both $directory and $uri. Recommended to add the resulting file
     * path to .gitignore so published vendor assets will not be tracked.
     *
     * @var string
     */
    public $vendor = 'vendor/';

    /**
     * Whether to append file modification timestamps on asset tags.
     * Makes it less likely for modified assets to remain cached.
     *
     * @var bool
     */
    public $useTimestamps = true;

    /**
     * Whether to cache bundles for faster route responses.
     *
     * @var bool
     */
    public $useCache = ENVIRONMENT === 'production';

    //--------------------------------------------------------------------
    // Route Assets
    //--------------------------------------------------------------------

    public $routes = [
        '*' => [
            'https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js',
            '/js/main.js'
        ],
        'users' => [
            // 'adminlte3/plugins/fontawesome-free/css/all.min.css',
            // 'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
            // 'adminlte3/plugins/icheck-bootstrap/icheck-bootstrap.min.css',
            // 'adminlte3/css/adminlte.min.css',

            // 'adminlte3/plugins/jquery/jquery.min.js',
            // 'adminlte3/plugins/jquery-ui/jquery-ui.min.js',
            // 'adminlte3/plugins/bootstrap/js/bootstrap.bundle.min.js',
            // '/js/main.js'
        ],
        'stores' => [
            // '/js/main.js'
        ],
        'register/*' => [
            \App\Bundles\BootstrapBundle::class,
        ],
    ];
}