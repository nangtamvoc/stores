<?php namespace Config;

use App\Filters\AuthFilter;
use CodeIgniter\Config\BaseConfig;
use CodeIgniter\Filters\CSRF;
use CodeIgniter\Filters\DebugToolbar;
use CodeIgniter\Filters\Honeypot;

use Tatter\Assets\Filters\AssetsFilter;

class Filters extends BaseConfig
{
	// Makes reading things below nicer,
	// and simpler to change out script that's used.
	public $aliases = [
		'csrf'     => CSRF::class,
		'toolbar'  => DebugToolbar::class,
		'honeypot' => Honeypot::class,
		'assets'   => AssetsFilter::class,
		'myauth'   => AuthFilter::class
	];

	// Always applied before every request
	public $globals = [
		'before' => [
			//'honeypot'
			'csrf' =>[
				'except' => [
					'users/*', 'users',
					'stores/update_assign',
					'stores/getDistricts',
					'stores/getWards',
					'stores/upload_image',
					'stores/delImageSession',
					'report/get_stores_detail'
				]
			],
		],
		'after'  => [
			'toolbar',
			//'honeypot',
			'assets' => ['except' => 'api/*'],
		],
	];

	// Works on all of a particular HTTP method
	// (GET, POST, etc) as BEFORE filters only
	//     like: 'post' => ['CSRF', 'throttle'],
	public $methods = [];

	// List filter aliases and any before/after uri patterns
	// that they should run on, like:
	//    'isLoggedIn' => ['before' => ['account/*', 'profiles/*']],
	public $filters = [
		'myauth'	=> ['before' => ['dashboard', 'users', 'stores/*', 'report/*']]
	];
}
