<?php namespace App\Controllers;

class Login extends BaseController
{
	public function index()
	{
		$this->data['title'] = "Nuti Store | Đăng nhập";
		if ($this->request->getMethod() == "post") {
			$username = $this->request->getPost('username');
            $password = $this->request->getPost('password');
			$data = [
				'username' => $username,
				'password' => $password
			];
			$response 		= executeApi('MbUser/login', HTTP_POST, $data);
			if ($response['success']) {
                $session_data = [
                    'isLoggedIn' => 1,
                    'token'   => $response['data']['Token'],
                    'user' 	  => $response['data'],
                ];
                session()->set($session_data);
				// if(!empty($this->request->getPost("remember"))){
				// 	$expire 	= 60 * 60 * 24 * 2;
					// set_cookie("username", $username, 36000, '/');
				// 	set_cookie("remember_code", $password, $expire, '/');

				// }else{
				// 	set_cookie('remember_code', '');
				// 	set_cookie('username', '');
				// }
				// store a cookie value
				// set_cookie("abc", "anc", 3600);

				// get cookie value

				// remove cookie value
				// delete_cookie("username");

                return redirect()->to('/dashboard');
            } else {
                session()->setFlashdata('error', $response['message']);
                return redirect()->to('/');
            }
		}
    	return view('pages/login', $this->data);
	}

	function logout()
    {
        session()->destroy();
        return redirect()->to('/');
    }
}
