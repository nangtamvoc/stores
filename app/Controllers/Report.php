<?php namespace App\Controllers;

use CodeIgniter\I18n\Time;
class Report extends BaseController
{

	public function index(){
		return;
	}

	function list_store(){
		$this->data['title'] = "Nuti Store | Theo dõi mặt bằng";
		$this->data['breadcrumb_title'] = "Theo dõi mặt bằng";
		$breadcrumb =   array(
							array(
								'title' => 'Trang chủ',
								'link' => 'dashboard'
							),
							array(
								'title' => 'Theo dõi mặt bằng',
								'link' => null
							)
						);
		$this->data['breadcrumb'] = $breadcrumb;

		# Người chịu trách nhiệm
		$responseEmp 		= executeApi('MbUser/get-emp', HTTP_GET, [
			'FromIndex'		=> 0,
			'ToIndex'		=> 999
		]);
		$dataEmp = [];
		if($responseEmp['success']){
			$dataEmp['ALL'] = 'Chọn người chịu trách nhiệm';
			foreach($responseEmp['data'] as $val){
				$dataEmp[$val['UserName']]	= $val['FullName'];
			}
		}
		$this->data['emps'] = $dataEmp;
		# Tỉnh thành
		$this->data['provinceList'] = getListProvinceDistrictWard('province', 'ALL', 'ALL');

		$todate = date('d/m/Y');
		$fromdate = strftime("%d/%m/%Y", strtotime(date("d-m-Y", strtotime( date('d-m-Y') )) . " -20 days"));
		$this->data['reservation']  =  $fromdate. ' - ' . $todate ;

		return view('report/list', $this->data);
	}

	function get_stores(){
		$start			= $this->request->getGet('start') ?? 0;
		$length 		= (empty($this->request->getGet('length')) || $this->request->getGet('length') == 'NaN')?999:$this->request->getGet('length');
		$search 		= $this->request->getGet('search')['value'] ?? null;

		$reservation 	= $this->request->getGet('reservation') ?? '';
		$todate = $fromdate = '';
		if(!empty($reservation)):
			$fromdate 	= Time::createFromFormat('d/m/Y', substr($reservation, 0, 10))->toDateString();
			$todate 	= Time::createFromFormat('d/m/Y', substr($reservation, -10, 10))->toDateString();
		endif;
		$province = (!empty($this->request->getGet('province')))? $this->request->getGet('province') : 'ALL';
		$district = (!empty($this->request->getGet('district')))? $this->request->getGet('district') : 'ALL';
		$ward 	  = (!empty($this->request->getGet('ward')))? $this->request->getGet('ward') : 'ALL';
		$assign   = (!empty($this->request->getGet('assign')))? $this->request->getGet('assign') : 'ALL';

		$finalResponse = array(
			'success' => false,
			'code' => 400,
			'message' => '',
			'draw' => intval($this->request->getGet('draw') ?? 0),
			'recordsTotal' => 0,
			'recordsFiltered' => 0,
			'start'	=> $start,
			'length'=> $length,
			'data' => [],
			'sutest'	=> $_REQUEST,
			'to' => $todate,
			'from' => $fromdate,
			'csrf_nutistore_name'	=> csrf_hash()
		);
		$data = [
			'FromIndex' 	=> $start,
			'ToIndex' 		=> $length,
			'Assign' 		=> $assign,
			'FromDate'		=> $fromdate,
			'ToDate'		=> $todate,
			'ProvinceCode'	=> $province,
			'DistrictCode'	=> $district,
			'WardCode'		=> $ward

		];
		$response 		= executeApi('MbReport/report-mb-shop', HTTP_GET, $data);
		$finalResponse['data'] = $response['data'];
		$finalResponse['message'] = $response['message'];
		$finalResponse['code'] = $response['code'];
		$finalResponse['success'] = $response['success'];
		if (count($finalResponse['data'])>0) {
			$finalResponse['recordsTotal'] = count($response['data']);
			$finalResponse['recordsFiltered'] = count($response['data']);
		}
		echo json_encode($finalResponse);
	}

	function get_stores_detail(){
		if ($this->request->isAJAX()) {
			$finalResponse 	= array(
				'success' 	=> false,
				'code' 		=> 400,
				'message' 	=> '',
			);
            $view = \Config\Services::renderer();
			$id	  = $this->request->getPost('id') ?? 0;
            $data = [
				'id' => $id
			];
			$response 			  = executeApi('MbShop/get-by-id', HTTP_GET, $data);
            try {
                if ($response['success'] == true) {
					$finalResponse['stores'] 	= $response['data'];
					$finalResponse['message'] 	= 'OK';
					$finalResponse['code'] 		= $response['code'];
					$finalResponse['success'] 	= $response['success'];
                } else {
                    $finalResponse['message'] = 'Đã có lỗi xảy ra! Vui lòng thử lại';
                }
            } catch (\Exception $e) {
                $finalResponse['message'] = 'Đã có lỗi xảy ra! Vui lòng thử lại';
                log_message("error", print_r($e->getMessage(), true));
            }
            echo $view->setData($finalResponse)->render('report/detailModal');
        }
	}



	public function chartjs(){
		$this->data['title'] = "Nuti Store | Báo cáo";
		$this->data['breadcrumb_title'] = "ChartJS";
		$breadcrumb =   array(
				array(
					'title' => 'Home',
					'link' => 'dashboard'
				),
				array(
					'title' => 'ChartJS',
					'link' => null
				)
			);
		$this->data['breadcrumb'] = $breadcrumb;
		return view('charts/chartjs', $this->data);
	}

	public function flot(){
		$this->data['title'] = "Nuti Store | Flot Charts";
		$this->data['breadcrumb_title'] = "Flot Charts";
		$breadcrumb =   array(
							array(
								'title' => 'Home',
								'link' => 'dashboard'
							),
							array(
								'title' => 'Flot',
								'link' => null
							)
						);
		$this->data['breadcrumb'] = $breadcrumb;
		return view('charts/flot', $this->data);
	}

	public function inline(){
		$this->data['title'] = "Nuti Store | Inline Charts";
		$this->data['breadcrumb_title'] = "Inline Charts";
		$breadcrumb =   array(
							array(
								'title' => 'Home',
								'link' => 'dashboard'
							),
							array(
								'title' => 'Inline Charts',
								'link' => null
							)
						);
		$this->data['breadcrumb'] = $breadcrumb;
		return view('charts/inline', $this->data);
	}
}