<?php namespace App\Controllers;
use CodeIgniter\I18n\Time;
class Stores extends BaseController
{

	public function index(){
		$this->data['title'] = "Nuti Store | Danh sách mặt bằng";
		$this->data['breadcrumb_title'] = "Danh sách mặt bằng";
		$breadcrumb =   array(
							array(
								'title' => 'Trang chủ',
								'link' => 'dashboard'
							),
							array(
								'title' => 'Mặt bằng',
								'link' => null
							)
						);
		$this->data['breadcrumb'] = $breadcrumb;
		if ($this->request->getMethod() == "post") {
			
		}else{

			# Người chịu trách nhiệm
			$responseEmp 		= executeApi('MbUser/get-emp', HTTP_GET, [
				'FromIndex'		=> 0,
				'ToIndex'		=> 999
			]);
			$dataEmp = [];
			if($responseEmp['success']){
				$dataEmp['ALL'] = 'Chọn người chịu trách nhiệm';
				foreach($responseEmp['data'] as $val){
					$dataEmp[$val['UserName']]	= $val['FullName'];
				}
			}
			$this->data['emps'] = $dataEmp;
			# Tỉnh thành
			$this->data['provinceList'] = getListProvinceDistrictWard('province', 'ALL', 'ALL');

			$todate = date('d/m/Y');
			$fromdate = strftime("%d/%m/%Y", strtotime(date("d-m-Y", strtotime( date('d-m-Y') )) . " -20 days"));
			$this->data['reservation']  =  $fromdate. ' - ' . $todate ;

			return view('stores/index', $this->data);
		}
	}

	function get_stores(){
			$start			= $this->request->getGet('start') ?? 0;
			$length 		= (empty($this->request->getGet('length')) || $this->request->getGet('length') == 'NaN')?999:$this->request->getGet('length');
			$search 		= $this->request->getGet('search')['value'] ?? null;

			$reservation 	= $this->request->getGet('reservation') ?? '';
			$todate = $fromdate = '';
			if(!empty($reservation)):
				$fromdate 	= Time::createFromFormat('d/m/Y', substr($reservation, 0, 10))->toDateString();
				$todate 	= Time::createFromFormat('d/m/Y', substr($reservation, -10, 10))->toDateString();
			endif;
			$province = (!empty($this->request->getGet('province')))? $this->request->getGet('province') : 'ALL';
			$district = (!empty($this->request->getGet('district')))? $this->request->getGet('district') : 'ALL';
			$ward 	  = (!empty($this->request->getGet('ward')))? $this->request->getGet('ward') : 'ALL';
			$assign   = (!empty($this->request->getGet('assign')))? $this->request->getGet('assign') : 'ALL';

			$finalResponse = array(
				'success' => false,
				'code' => 400,
				'message' => '',
				'draw' => intval($this->request->getGet('draw') ?? 0),
				'recordsTotal' => 0,
				'recordsFiltered' => 0,
				'start'	=> $start,
				'length'=> $length,
				'data' => [],
				'sutest'	=> $_REQUEST,
				'to' => $todate,
				'from' => $fromdate,
				'csrf_nutistore_name'	=> csrf_hash()
			);
			$data = [
				'FromIndex' 	=> $start,
				'ToIndex' 		=> $length,
				'Assign' 		=> $assign,
				'FromDate'		=> $fromdate,
				'ToDate'		=> $todate,
				'ProvinceCode'	=> $province,
				'DistrictCode'	=> $district,
				'WardCode'		=> $ward

			];
			$response 		= executeApi('MbShop/get', HTTP_GET, $data);
			$finalResponse['data'] = $response['data'];
			$finalResponse['message'] = $response['message'];
			$finalResponse['code'] = $response['code'];
			$finalResponse['success'] = $response['success'];
			if (count($finalResponse['data'])>0) {
				$finalResponse['recordsTotal'] = count($response['data']);
				$finalResponse['recordsFiltered'] = count($response['data']);
			}
			echo json_encode($finalResponse);
	}

	public function add(){
		$this->data['title'] = "Nuti Store | Thêm mặt bằng";
		$this->data['breadcrumb_title'] = "Thêm mặt bằng";
		$breadcrumb =   array(
			array(
				'title' => 'Trang chủ',
				'link' => 'dashboard'
			),
			array(
				'title' => 'Mặt bằng',
				'link' => 'stores'
			)
		);
		$this->data['breadcrumb'] = $breadcrumb;
		$this->data['provinceList'] = getListProvinceDistrictWard('province', 'ALL', 'ALL');

		if( session()->get('user')['User']['RoleId'] == 2 ):
			$dataEmp 					= [];
			$responseEmp 				= executeApi('MbUser/get-emp', HTTP_GET, ['FromIndex' => 0, 'ToIndex' => 999]);
			if(!empty($responseEmp['data'])){
				$dataEmp[''] 			= 'Vui lòng chọn';
				foreach($responseEmp['data'] as $value){
					$dataEmp[$value['UserName']] 	= $value['FullName'];
				}
			}
			$this->data['dataEmp']  	= $dataEmp;
		endif;
		return view('stores/add', $this->data);
	}

	public function edit($id){
		$this->data['title'] = "Nuti Store | Cập nhật mặt bằng";
		$this->data['breadcrumb_title'] = "Cập nhật mặt bằng";
		$breadcrumb =   array(
			array(
				'title' => 'Trang chủ',
				'link' => 'dashboard'
			),
			array(
				'title' => 'Mặt bằng',
				'link' => 'stores'
			)
		);
		$this->data['breadcrumb'] = $breadcrumb;
		$data = [
			'id' => $id
		];
		$response 					= executeApi('MbShop/get-by-id', HTTP_GET, $data);
		$this->data['stores'] 		= $response['data'];

		$this->data['provinceList'] = getListProvinceDistrictWard('province', 'ALL', 'ALL');

		$provinceCode = $this->data['stores']['ProvinceCode'];
        $this->data['districtList'] = getListProvinceDistrictWard('district', 'ALL', $provinceCode);

		$districtCode = $this->data['stores']['DistrictCode'];
		$this->data['wardList']  	= getListProvinceDistrictWard('ward', 'ALL', $districtCode);

		if( session()->get('user')['User']['RoleId'] == 2 ):
			$dataEmp 					= [];
			$responseEmp 				= executeApi('MbUser/get-emp', HTTP_GET, ['FromIndex' => 0, 'ToIndex' => 999]);
			if(!empty($responseEmp['data'])){
				$dataEmp[''] 			= 'Vui lòng chọn';
				foreach($responseEmp['data'] as $value){
					$dataEmp[$value['UserName']] 	= $value['FullName'];
				}
			}
			$this->data['dataEmp']  	= $dataEmp;
		endif;

		return view('stores/edit', $this->data);
	}

	public function review($id){
		$this->data['title'] = "Nuti Store | Đánh giá mặt bằng";
		
		$breadcrumb =   array(
							array(
								'title' => 'Trang chủ',
								'link' => 'dashboard'
							),
							array(
								'title' => 'Mặt bằng',
								'link' => 'stores'
							)
						);
		$this->data['breadcrumb'] = $breadcrumb;


		$data = [
			'id' => $id
		];
		$response 		= executeApi('MbShop/get-by-id', HTTP_GET, $data);
		$this->data['stores'] = $response['data'];

		$this->data['provinceList'] = getListProvinceDistrictWard('province', 'ALL', 'ALL');

		$provinceCode = $this->data['stores']['ProvinceCode'];
        $this->data['districtList'] = getListProvinceDistrictWard('district', 'ALL', $provinceCode);

		$districtCode = $this->data['stores']['DistrictCode'];
		$this->data['wardList']  	= getListProvinceDistrictWard('ward', 'ALL', $districtCode);

		$this->data['breadcrumb_title'] = "Đánh giá mặt bằng: ". $this->data['stores']['ShopName'];
		return view('stores/review', $this->data);
	}

	public function delSession(){

	}

	public function submitStore(){
		if ($this->request->getMethod() == "post") {
			$type 		= $this->request->getPost("type") ?? 'add';
			if($type != 'review'){
				$shopcode 	= $this->request->getPost("shopcode") ?? '';
				$shopname 	= $this->request->getPost("shopname") ?? '';
				$tel  		= $this->request->getPost("tel") ?? '';
				$email 		= $this->request->getPost("email") ?? '';
				$note		= $this->request->getPost("note") ?? '';
				$opendate	= $this->request->getPost("opendate") ?? '';
				$address	= $this->request->getPost("address") ?? '';
				$provincecode	= $this->request->getPost("provincecode") ?? '';
				$districtcode	= $this->request->getPost("districtcode") ?? '';
				$wardcode	= $this->request->getPost("wardcode") ?? '';
				$lon	= $this->request->getPost("lon") ?? '';
				$lat	= $this->request->getPost("lat") ?? '';
				$assign	= $this->request->getPost("assign") ?? '';

				if (!empty($opendate)) {
					$date = new \DateTime($opendate);
					$opendate = $date->format('Y-m-d');
				} else {
					$opendate = null;
				}
				$data = [
					"ShopCode"		=> $shopcode,
					"ShopName"		=> $shopname,
					"CusName"		=> '',
					"Tel"			=> $tel,
					"Email"			=> $email,
					"Address"		=> $address,
					"WardCode"		=> $wardcode,
					"DistrictCode"	=> $districtcode,
					"ProvinceCode"	=> $provincecode,
					"Lng"			=> $lon,
					"Lat"			=> $lat,
					"OpenDate"		=> $opendate,
					"Assign"		=> $assign,
					"Note"			=> $note,
					"Owner"			=> '',
					"Status"		=> 1,
					"CreateBy"		=> session()->get('user')['User']['UserName']
				];
			}else{
				$MsId 	= $this->request->getPost("MsId") ?? '';
				$data 	= [
					'id' => $MsId
				];
				$responseMbShopGetID  = executeApi('MbShop/get-by-id', HTTP_GET, $data);
				$stores = $responseMbShopGetID['data'];
			}

			if($type == 'add') {
				$response 		= executeApi('MbShop/add', HTTP_POST, $data);
				$textMessage 	= 'Thêm mặt bằng thành công';
				if($response['success']){
					session()->setFlashdata('success', $textMessage);
				}else{
					session()->setFlashdata('error', 'Đã có lỗi xảy ra vui lòng thử lại sau');
				}
				return redirect()->to('stores');
			}elseif($type == 'update') {
				$MsId 				= $this->request->getPost("MsId") ?? '';
				$data['MsId'] 		= $MsId;
				$status 			= $this->request->getPost("status") ?? 1;
				$data['Status'] 	= $status;
				unset($data['CreateBy']);
				$data['UpdateBy']	= session()->get('user')['User']['UserName'];

				$response 			= executeApi('MbShop/update', HTTP_PUT, $data);
				$textMessage 		= 'Cập nhật mặt bằng thành công';
				if($response['success']){
					session()->setFlashdata('success', $textMessage);
				}else{
					session()->setFlashdata('error', 'Đã có lỗi xảy ra vui lòng thử lại sau');
				}
				return redirect()->to('stores');
			}elseif($type == 'review') {
				$dataReview = [
					"MsId" => $MsId,
					"ShopCode" => $stores['ShopCode'],
					"ShopName" => $stores['ShopName'],
					"CusName" => $stores['CusName'],
					"Tel" => $stores['Tel'],
					"Email"=> $stores['Email'],
					"Address"=> $stores['Address'],
					"WardCode"=> $stores['WardCode'], 
					"DistrictCode"=> $stores['DistrictCode'],
					"ProvinceCode"=> $stores['ProvinceCode'],
					"Lng"=> $stores['Lng'], 
					"Lat"=> $stores['Lat'],
					"OpenDate"=> $stores['OpenDate'],
					"Assign"=> $stores['Assign'],
					"Note"=> $stores['Note'],
					"Owner"=> $stores['Owner'],
					"Status"=> 1,
					"UpdateBy"=>  session()->get('user')['User']['UserName']
				];

				if(!empty($stores['ShopFormula']['ShopFormulaDetail'])):
                    $array_new = $stores['ShopFormula']['ShopFormulaDetail'];
					$dataUpdate = [];
					$image_del  = [];
					$i = 0;
                    foreach($array_new as $k => $val) :
						foreach($val['Items'] as $key=>$val2):
							$MsfdId = $val2['MsfdId'];

							$image = '';
							if (session()->has('imagenew_' . $MsfdId)) {
								$imagenew = session()->get('imagenew_' . $MsfdId);
								session()->remove('imagenew_'.$MsfdId);
								if (session()->has('image_current_' . $MsfdId)) {
									$image_current = session()->get('image_current_' . $MsfdId);
									session()->remove('image_current_'.$MsfdId);
									$image = array_merge($image_current, $imagenew);
								} else $image = $imagenew;

								$image = array_unique($image, SORT_REGULAR);
								$dataImage = array(
									'image_'.$MsfdId => json_encode($image)
								);
								session()->set($dataImage);
							}

							$image = '';
							if (session()->has('image_'.$MsfdId)) {
								$image = session()->get("image_".$MsfdId);
								// echo 'Array image mới: '.$MsfdId.' <br>' ;
								session()->remove('image_'.$MsfdId);
							} else {
								// echo 'Array image cũ: '.$MsfdId.' <br>' ;
								$image = session()->get('image_current_' . $MsfdId);
								$image = json_encode($image);
								session()->remove('image_current_'.$MsfdId);
							}
							// echo "<pre>";
							
							// print_r($image);
							// echo '<br>';

							// echo "<pre>";
							// echo 'Array xoá: <br>';
							if (session()->has('image_del_'.$MsfdId)) {
								$image_del = session()->get("image_del_".$MsfdId);
								session()->remove('image_del_'.$MsfdId);
								if (!empty($image_del)) {
									foreach ($image_del as $key => $value) {
										@unlink(ROOTPATH .'public/uploads/'.$value);
									}
								}
							}
							// echo '<br>';

							if(!empty($val2['ListValues'])) :
								$point = $this->request->getPost("point_".$MsfdId) ?? 0;
								$dataUpdate[] = [
									"MsfdId" => $MsfdId,
									"Point"=> (int)$point + 0,
									"Note"=> $val2['Note'],
									"Images"=> $image,
									"EmpResult"=> $val2['EmpResult'],
									"ManageResult"=> $val2['ManageResult'],
									"UpdateBy"=> session()->get('user')['User']['UserName']
								];
							else:
								$note 		= $this->request->getPost("note_".$MsfdId) ?? '';
								$empResult  = $this->request->getPost("empResult_".$MsfdId) ?? $val2['EmpResult'];
								$manageResult = $this->request->getPost("manageResult_".$MsfdId) ?? $val2['ManageResult'];
								
								if(session()->get('user')['User']['RoleId'] == 1){
									if($empResult == 'Đạt'){
										$i++;
									}
								} else {
									if($manageResult == 'Đạt'){
										$i++;
									}
								}
								$dataUpdate[] = [
									"MsfdId" => $MsfdId,
									"Point"=> $val2['Point'],
									"Note"=> $note,
									"Images"=> $image,
									"EmpResult"=> $empResult,
									"ManageResult"=> $manageResult,
									"UpdateBy"=> session()->get('user')['User']['UserName']
								];
							endif;

						endforeach;
					endforeach;
					$dataReview['Details'] = $dataUpdate;
					if($i == 10){
						$dataReview['Status'] = (session()->get('user')['User']['RoleId'] == 1)?5:9;
						# role = 1
						# EmpResult
						# 1-9 => so 3
						# 10 => so 5

						# role = 2
						# ManageResult
						# 1-9 => so 7
						# 10 => 9
					}else{
						$dataReview['Status'] = (session()->get('user')['User']['RoleId'] == 1)?3:7;
					}
				endif;

				// echo "<pre>";
				// echo 'Array cạp nhật: <br>';
				// print_r($dataUpdate);
				
				
				// exit;

				$response 		= executeApi('MbShop/update-full', HTTP_PUT, $dataReview);
				$textMessage = 'Chấm điểm mặt bằng thành công';

				// echo json_encode($response);

				if($response['success']){
					
					session()->setFlashdata('success', $textMessage);
				}else{
					session()->setFlashdata('error', 'Đã có lỗi xảy ra vui lòng thử lại sau');
				}
				return redirect()->to('stores');
			}
			
		}
	}	
	
	function update_assign(){
		$msid 			= $this->request->getPost("msid") ?? '';
		$assign 		= $this->request->getPost("assign") ?? '';
		$finalResponse 	= array(
			'success' 	=> false,
			'code' 		=> 400,
			'message' 	=> '',
		);
		if(!empty($msid) && !empty($assign)){
			$response 		= executeApi('MbShop/assign', HTTP_PUT, [
				"MsId" 		=> $msid,
				"Assign"	=> $assign,
				"UpdateBy"	=> session()->get('user')['User']['UserName']
			]);
			if($response['success']){
				$finalResponse['data'] 		= $response['data'];
				$finalResponse['message'] 	= 'Thay đổi người phụ trách thành công';
				$finalResponse['code'] 		= $response['code'];
				$finalResponse['success'] 	= $response['success'];
			}
		}
		return json_encode($finalResponse);
	}

	function upload_image()
    {
        sleep(1);
		$shopid = $this->request->getPost("shopid");
		$msfdid = $this->request->getPost("msfdid");
		$output = '';
        if ($imagefile = $this->request->getFiles()) {

			if(session()->has('image_current_'.$msfdid)):
                $image_current = session()->get('image_current_'.$msfdid);
                foreach ($image_current as $key => $value) {
                    $output .= '<div class="col-sm-3"><a msfdid="' .$msfdid. '" shopid="' . $shopid . '" onclick="del_image(this)" name_image="' . $value . '" class="del_image" title="Xóa" class="btn btn-default"><i class="fas fa-trash-alt"></i> Xoá</a><img src="' . base_url() . '\/uploads\/' . $value . '?r=' . time() . '" class="img-responsive img-thumbnail" data-action="zoom" /></div>';
                }
            endif;

            $arrImage = [];
			foreach($imagefile['files'] as $k=>$img) {
				if ($img->isValid() && ! $img->hasMoved()) {
					$newName = $img->getRandomName();
					$img->move(ROOTPATH .'public/uploads', $newName);
					$arrImage[$k] = $newName;
				}
			}
			if (!empty($arrImage)) {
				if (session()->has('imagenew_' . $msfdid)) {
                    $imagenew = session()->get('imagenew_' . $msfdid);
                    $arrImage = array_merge($arrImage, $imagenew);
                }
				session()->set('imagenew_'.$msfdid, $arrImage);
				foreach($arrImage as $k=>$value){
					$output .= '<div class="col-sm-3"><a msfdid="' .$msfdid. '" shopid="' . $shopid . '" onclick="del_image(this)" name_image="' . $value . '" class="del_image" title="Xóa" class="btn btn-default"><i class="fas fa-trash-alt"></i> Xoá</a><img src="' . base_url() .'\/uploads\/'. $value . '?r=' . time() . '" class="img-responsive img-thumbnail" data-action="zoom" /></div>';
				}
			}
			
            echo $output;
        }
    }

	function delImageSession()
    {
        $shopid = $this->request->getPost('shopid');
        $name_img = $this->request->getPost('name_img');
		$msfdid = $this->request->getPost("msfdid");
        if (session()->has('image_current_'.$msfdid)) {
            $str_array = json_encode(session()->get('image_current_'.$msfdid));
			session()->remove('image_current_'.$msfdid);
            $str_array = str_replace($name_img, "", $str_array);
            $str_array = str_replace('"",', "", $str_array);
            $str_array = str_replace(',""', "", $str_array);
            $str_array = str_replace('""', "", $str_array);
        } else $str_array = json_encode([]);

        $image_arr_current = json_decode($str_array);
		if(session()->has('imagenew_'.$msfdid)){
			$str_arrayNew = json_encode(session()->get('imagenew_'.$msfdid));
			session()->remove('imagenew_'.$msfdid);
            $str_arrayNew = str_replace($name_img, "", $str_arrayNew);
            $str_arrayNew = str_replace('"",', "", $str_arrayNew);
            $str_arrayNew = str_replace(',""', "", $str_arrayNew);
            $str_arrayNew = str_replace('""', "", $str_arrayNew);
            $str_arrayNew = json_decode($str_arrayNew);
			session()->set('imagenew_'.$msfdid, $str_arrayNew);
            $image_arr_current = array_unique(array_merge($image_arr_current, $str_arrayNew), SORT_REGULAR);
        }
		$image_current = array(
			'image_current_'.$msfdid => $image_arr_current
		);
		session()->set($image_current);
        # Mảng xóa
		if(session()->has('image_del_'.$msfdid)){
			$m = session()->get('image_del_'.$msfdid);
			session()->remove('image_del_'.$msfdid);
            array_push($m, $name_img);
			$image_del = array(
				'image_del_'.$msfdid => $m
			);
			session()->set($image_del);
        } else {
			$image_del = array(
				'image_del_'.$msfdid => [$name_img]
			);
			session()->set($image_del);
        }

        $output = "";
        if (!empty($image_arr_current)):
            foreach ($image_arr_current as $key => $value) {
                $output .= '
                <div class="col-sm-3">
                <a msfdid="' .$msfdid. '" shopid="' . $shopid . '" onclick="del_image(this)" name_image="' . $value . '" class="del_image btn btn-default" title="Xóa"><i class="fas fa-trash-alt"></i> Xoá</a>
                <img src="' . base_url() . '\/uploads\/' . $value . '" class="img-responsive img-thumbnail" data-action="zoom" />
                </div>
                ';
            }
        endif;
        echo $output;
    }

	public function getDistricts(){
        if ($this->request->isAJAX()) {
            $provinceCode = $this->request->getPost('provinceCode');
            $finalResponse['data'] = getListProvinceDistrictWard('district', 'ALL', $provinceCode);
            $finalResponse['success'] = true;
            return json_encode($finalResponse);
        }
    }

	public function getWards(){
        if ($this->request->isAJAX()) {
            $districtCode = $this->request->getPost('districtCode');
            $finalResponse['data'] = getListProvinceDistrictWard('ward', 'ALL', $districtCode);
            $finalResponse['success'] = true;
            return json_encode($finalResponse);
        }
    }

}
