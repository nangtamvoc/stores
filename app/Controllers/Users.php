<?php namespace App\Controllers;

class Users extends BaseController
{

	public function index(){
		$this->data['title'] = "Nuti Store | Danh sách người dùng";
		$this->data['breadcrumb_title'] = "Danh sách người dùng";
		$breadcrumb =   array(
							array(
								'title' => 'Trang chủ',
								'link' => 'dashboard'
							),
							array(
								'title' => 'Người dùng',
								'link' => null
							)
						);
		$this->data['breadcrumb'] = $breadcrumb;
		if ($this->request->getMethod() == "post") {
			$start = $this->request->getPostGet('start') + 0 ?? 0;
			$length = $start + ($this->request->getPostGet('length') ?? 999);
			$finalResponse = array(
				'success' => false,
				'code' => 400,
				'message' => '',
				'draw' => intval($this->request->getPostGet('draw') ?? 0),
				// 'recordsTotal' => 0,
				// 'recordsFiltered' => 0,
				// 'start'	=> $start,
				// 'length'	=> $length,
				'data' => [],
			);
			$data = [
				'FromIndex' => $start,
				'ToIndex' => $length
			];
			$response 		= executeApi('MbUser/get', HTTP_GET, $data);
			$finalResponse['data'] = $response['data'];
			$finalResponse['message'] = $response['message'];
			$finalResponse['code'] = $response['code'];
			$finalResponse['success'] = $response['success'];
			if (count($finalResponse['data'])>0) {
				$finalResponse['recordsTotal'] = count($response['data']);
				$finalResponse['recordsFiltered'] = count($response['data']);
			}
			echo json_encode($finalResponse);
		}else{
			return view('users/index', $this->data);
		}
	}

	public function submitUser(){
		if ($this->request->getMethod() == "post") {
			$finalResponse = array(
				'success' => false,	
				'code' => 400,
				'message' => '',
				'data' => [],
			);
			$username 	= $this->request->getPost("userName") ?? '';
			$type 		= $this->request->getPost("type") ?? 'add';
			$password 	= $this->request->getPost("password") ?? '';
			$fullName 	= $this->request->getPost("fullName") ?? '';
			$email 		= $this->request->getPost("email") ?? '';
			$phoneNumber= $this->request->getPost("phoneNumber") ?? '';
			$data = [
				"UserName"		=> $username,
				"Password"		=> $password,
				"FullName"		=> $fullName,
				"Email"			=> $email,
				"PhoneNumber"	=> $phoneNumber
			];
			if($type == 'add'){
				$response 		= executeApi('MbUser/add', HTTP_POST, $data);
			}else{
				$response 		= executeApi('MbUser/update', HTTP_PUT, $data);
			}
			// $finalResponse['data'] = $response['data'];
			// $finalResponse['message'] = $response['message'];
			// $finalResponse['code'] = $response['code'];
			// $finalResponse['success'] = $response['success'];
			// $finalResponse['data'] = $dataInsert;
			
			echo json_encode(array_merge($finalResponse, $response));
		}
	}

}
