<?php
use Config\Services;
/**
 * Tài khoản nhân viên
 * userName: thanhd,
 * password: nutidev@nuti
 * 
 * Tài khoản quản lý
 * userName: thanhd1
 * password": nutidev@nuti
 */

if(!function_exists("executeApi")){

    function executeApi($url, $method, $data = [])
    {
        log_message('debug', 'Start call API:');
        $result = array(
            'success' => false,
            'code' => 500,
            'message' => '',
            'data' => [],
        );
        try {
            if (ENVIRONMENT == 'production') {
                $host = 'http://113.161.105.195:6666/api/';
            } else {
                if (ENVIRONMENT == 'development' && $_SERVER['SERVER_NAME'] == 's.svleague.vn') {
                    $host = 'http://113.161.105.195:6668/api/';
                } else {
                    // $host = 'http://192.168.0.221:6666/api/';
                    $host = 'http://192.168.0.221:6668/api/';
                }
            }
            log_message('debug', 'Start call API: '.$host.$url.'. Method: '.$method);
            $token = session()->get('token') ?? '';
            log_message('debug', 'Token: ' . $token);
            log_message('debug', 'Params: ' . print_r($data, true));
            
            $client = Services::curlrequest([
                'baseURI' => $host,
                // 'timeout'  => 3,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'charset' => 'utf-8',
                    'Authorization' => 'Bearer ' . $token,
                    'Client-IPAddress'    => getClientIpAddress()
                ],
                'defaults' => [
                    'exceptions' => false,
                ],
                'http_errors' => false,
            ]);
            $options = [];
            switch ($method) {
                case HTTP_GET:
                case HTTP_DELETE:
                    $options = ['query' => $data];
                    break;
                case HTTP_PUT:
                case HTTP_POST:
                    $options = ['json' => $data];
                    break;
                case HTTP_FILE:
                    $options = [
                        'multipart' => [
                            [
                                'name' => $data['formFile'],
                                'contents' => file_get_contents($data['pathname']),
                                'filename' => $data['filename']
                            ]
                        ],
                    ];
                    break;
            }

            $response           = $client->request($method, $url, $options);
            log_message('debug', 'response: ' .print_r(($response), true));
            $httpCode           = $response->getStatusCode();
            $responseData       = json_decode($response->getBody(), true);
            $result['success']  = $responseData['Success'];
            $result['code']     = $httpCode;
            $result['message']  = $responseData['Message'] ?? '';
            $result['data']     = $responseData['Payload'] ?? [];

            log_message('debug', 'Body response: ' .print_r(json_decode($response->getBody()), true));
            log_message('debug', 'End call API: '.$host.$url.', method : '.$method);

        } catch (\Exception $e) {
            $result['message'] = $e->getMessage();
            log_message('debug', 'Exception API: '. print_r($e->getMessage(), true));
        }
        return $result;
    }

}


if(!function_exists("getClientIpAddress")){
  
    function getClientIpAddress()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //Checking IP From Shared Internet
        {
          $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //To Check IP is Pass From Proxy
        {
          $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
          $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }
}

if (!function_exists('getListProvinceDistrictWard')) {
    function getListProvinceDistrictWard( $type = '', $keyword = 'ALL', $id = 'ALL'){
        switch ($type) {
            case 'province':
                $url = 'Provinces/get-provinces';
                $dataPost = array(
                    'keyword'		=> $keyword,
                    'provinceId'	=> $id
                );
                $response 			= executeApi($url, HTTP_GET, $dataPost);
                try{
                    if($response['success'] == true){
                        $dataReturn	= $response['data'];
                        if(!empty($dataReturn)):
                            $finalResponse[''] = "Chọn tỉnh/thành phố...";
                            foreach ($dataReturn as $key => $value) {
                                $finalResponse[$value['ProvinceId']] = $value['ProvinceName'];
                            }
                        endif;
                    }else{
                        $finalResponse  = [];
                        log_message("error",  print_r($response['Message'], true));
                    }
                }catch(\Exception $e){
                    $finalResponse  = [];
                    log_message("error",  print_r($e->getMessage(), true));
                }
                break;
            case 'district':
                $url = 'Provinces/get-district-by-provinceId';
                $dataPost = array(
                    'keyword'		=> $keyword,
                    'provinceId'	=> $id
                );
                $response 			= executeApi($url, HTTP_GET, $dataPost);
                try{
                    if($response['success'] == true){
                        $dataReturn	= $response['data'];
                        if(!empty($dataReturn)):
                            $finalResponse[0] = "Chọn quận huyện...";
                            foreach ($dataReturn as $key => $value) {
                                $finalResponse[$value['DistrictId']] = $value['DistrictName'];
                            }
                        endif;
                    }else{
                        $finalResponse  = [];
                        log_message("error",  print_r($response['Message'], true));
                    }
                }catch(\Exception $e){
                    $finalResponse  = [];
                    log_message("error",  print_r($e->getMessage(), true));
                }
                break;

            case 'ward':
                $url = 'Provinces/get-ward-by-districtId';
                $dataPost = array(
                    'keyword'		=> $keyword,
                    'districtId'	=> $id
                );
                $response 			= executeApi($url, HTTP_GET, $dataPost);
                try{
                    if($response['success'] == true){
                        $dataReturn	= $response['data'];
                        if(!empty($dataReturn)):
                            $finalResponse[0] = "Chọn phường/xã...";
                            foreach ($dataReturn as $key => $value) {
                                $finalResponse[$value['WardId']] = $value['WardName'];
                            }
                        endif;
                    }else{
                        $finalResponse  = [];
                        log_message("error",  print_r($response['Message'], true));
                    }
                }catch(\Exception $e){
                    $finalResponse  = [];
                    log_message("error",  print_r($e->getMessage(), true));
                }
                # code...
                break;

            default:
                break;
        }
        
        return $finalResponse;
    }
}

if(!function_exists("formatDateTime")){
}
    function formatDateTime($ldate) {
    if ($ldate) {
        return date('d-m-Y H:i:s', strtotime($ldate));
    }
    return FALSE;
}

if(!function_exists("getStatusStores")){

    function getStatusStores($status) {
        if ($status == 0) {
            return '<span class="font-weight-bolder text-muted">Chưa kích hoạt</span>';
        }else if($status == 1) {
            return '<span class="font-weight-bolder text-info">Kích hoạt</span>';
        }else if($status == 3) {
            return '<span class="font-weight-bolder text-warning">Không đạt</span>';
        }else if($status == 5) {
            return '<span class="font-weight-bolder text-primary">Đạt</span>';
        }else if($status == 7) {
            return '<span class="font-weight-bolder text-danger">Không đạt</span>';
        }else if($status == 9) {
            return '<span class="font-weight-bolder text-success">Đạt</span>';
        }
        return FALSE;
    }
}

if(!function_exists("xepLoai")){

    function xepLoai($cou) {
        if ($cou >= 80 ) {
            return '<span class="font-weight-bolder text-muted">Xuất sắc</span>';
        }else if(70 <= $cou && $cou < 80) {
            return '<span class="font-weight-bolder text-info">Tốt</span>';
        }else if(60 <= $cou && $cou < 70 ) {
            return '<span class="font-weight-bolder text-warning">Khá</span>';
        }else if(50 <= $cou && $cou < 60) {
            return '<span class="font-weight-bolder text-primary">Trung bình</span>';
        }else if( 50 > $cou) {
            return '<span class="font-weight-bolder text-danger">Yếu</span>';
        }
    }
}

