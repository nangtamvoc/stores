<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title><?= $title ?></title>
      <!-- Set your app to full screen mode -->
      <meta name="apple-mobile-web-app-capable" content="yes">
      <!-- Set your app name -->
      <meta name="apple-mobile-web-app-title" content="MB System">
      <!-- Set app’s status bar style -->
      <meta name="apple-mobile-web-app-status-bar-style" content="black" />
      <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>/icon/logo_96x96.png">
      <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>/icon/logo_96x96.png">
      <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>/icon/logo_96x96.png">
      <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>/icon/logo_96x96.png">
      <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>/icon/logo_144x144.png">
      <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>/icon/logo_144x144.png">
      <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>/icon/logo_144x144.png">
      <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>/icon/logo_144x144.png">

      <link rel="apple-touch-startup-image" href="<?= base_url() ?>/icon/logo_324x324.png" media="screen resolution">
      <!-- For landscape mode -->
      <link rel="apple-touch-startup-image" href="<?= base_url() ?>/icon/logo_1024x1024.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)" />
      <!-- For portrait mode -->
      <link rel="apple-touch-startup-image" href="<?= base_url() ?>/icon/logo_1024x1024.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)" />
      <link rel="icon" type="image/png" href="<?= base_url() ?>/icon/logo_192x192.png" sizes="192x192">
      <link rel="icon" type="image/png" href="<?= base_url() ?>/icon/logo_96x96.png" sizes="96x96">
      <link rel="shortcut icon" href="<?= base_url() ?>/icon/logo_96x96.png">

      <!-- Set the viewport -->
      <meta content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no" name="viewport">
      
      <?= $this->include('layouts/head') ?>
    </head>
    <body class="hold-transition sidebar-mini layout-fixed">
      <div class="wrapper">
        <?= $this->include('layouts/navbar') ?>

        <?= $this->include('layouts/sidebar') ?>

        <div class="content-wrapper">
          <?= $this->include('layouts/breadcrumb') ?>
          <section class="content">
            <div class="container-fluid">

            <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
              <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
              </symbol>
              <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
              </symbol>
            </svg>

            <?php if (session()->getFlashdata('error') != null): ?>
                <div class="alert alert-danger d-flex align-items-center alert-dismissible fade show" role="alert">
                  <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                  <div>
                    <?= session()->getFlashdata('error') ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
                  </div>
                </div>
            <?php endif; ?>

            <?php if (session()->getFlashdata('success') != null): ?>
                <div class="alert alert-success d-flex align-items-center alert-dismissible fade show" role="alert">
                  <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                  <div>
                    <?= session()->getFlashdata('success') ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
                  </div>
                </div>
            <?php endif; ?>
            <?= $this->renderSection('content') ?>
            </div>
            <?= $this->renderSection('modals') ?>
          </section>
        </div>
        <footer class="main-footer">
          <?= $this->include('layouts/footer') ?>
        </footer>
        <aside class="control-sidebar control-sidebar-dark">
        </aside>
      </div>
      <?= $this->include('layouts/foot') ?>
    </body>
</html>
