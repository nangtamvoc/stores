<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $title ?></title>
  <!-- Set your app to full screen mode -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <!-- Set your app name -->
  <meta name="apple-mobile-web-app-title" content="MB System">
  <!-- Set app’s status bar style -->
  <meta name="apple-mobile-web-app-status-bar-style" content="black" />
  <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>/icon/logo_96x96.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>/icon/logo_96x96.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>/icon/logo_96x96.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>/icon/logo_96x96.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>/icon/logo_144x144.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>/icon/logo_144x144.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>/icon/logo_144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>/icon/logo_144x144.png">

  <link rel="apple-touch-startup-image" href="<?= base_url() ?>/icon/logo_324x324.png" media="screen resolution">
  <!-- For landscape mode -->
  <link rel="apple-touch-startup-image" href="<?= base_url() ?>/icon/logo_1024x1024.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)" />
  <!-- For portrait mode -->
  <link rel="apple-touch-startup-image" href="<?= base_url() ?>/icon/logo_1024x1024.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)" />
  <link rel="icon" type="image/png" href="<?= base_url() ?>/icon/logo_192x192.png" sizes="192x192">
  <link rel="icon" type="image/png" href="<?= base_url() ?>/icon/logo_96x96.png" sizes="96x96">
  <link rel="shortcut icon" href="<?= base_url() ?>/icon/logo_96x96.png">

  <!-- Set the viewport -->
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="<?= base_url('assets/adminlte3') ?>/plugins/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
  <link href="<?= base_url('assets/adminlte3') ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="<?= base_url('assets/adminlte3') ?>/css/adminlte.min.css" rel="stylesheet" type="text/css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- <div class="login-logo">
    <a href="<?= base_url('dashboard') ?>">
    <img src="<?= base_url() ?>/logo.jpg" alt="Nuti Store Logo" class="img-responsive img-thumbnail">
    <b>Nuti</b>Store
  </a>
  </div> -->
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
    <img src="<?= base_url() ?>/icon/logo_512x512.png" alt="Nuti Store Logo" class="img-responsive img-thumbnail">
      <p class="login-box-msg">
        Đăng nhập để bắt đầu làm việc với NutiStore
      </p>
      <?php if (session()->getFlashdata('error') != null): ?>
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <?= session()->getFlashdata('error') ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
          </div>
      <?php endif; ?>
      <?php 
      $attributes = ['class' => 'frmLogin', 'id' => 'frmLogin'];
        echo form_open(base_url('/login'), $attributes); 
      ?>
        <div class="input-group mb-3">
          <?= form_input([
              'name'      => 'username',
              'id'        => 'username',
              'value'     => '',
              'class'     => 'form-control',
              'placeholder'     => 'Tên đăng nhập',
          ]);?>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <?= form_password([
              'name'      => 'password',
              'id'        => 'password',
              'value'     => '',
              'class'     => 'form-control',
              'placeholder'     => 'Mật khẩu',
          ]);?>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-6">
            <!-- <div class="icheck-primary">
              <input name="remember" type="checkbox" id="remember" value="1" checked>
              <?= form_label('Nhớ tài khoản', 'remember')?>
            </div> -->
          </div>
          <!-- /.col -->
          <div class="col-6">
            <?php 
              $data = [
                'name'    => 'send',
                'id'      => 'send',
                'value'   => 'send',
                'type'    => 'submit',
                'class'   => 'btn btn-primary btn-block',
                'content' => 'Đăng nhập'
            ];
            echo form_button($data);
            ?>
          </div>
          
          <!-- /.col -->
        </div>
      <?= form_close();?>

      <!-- <p class="mb-1">
        <a href="<?= base_url('forgot-password') ?>">I forgot my password</a>
      </p>
      <p class="mb-0">
        <a href="<?= base_url('register') ?>" class="text-center">Register a new membership</a>
      </p> -->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<script src="<?= base_url('assets/adminlte3') ?>/plugins/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jquery-validation -->
<script src="<?= base_url('assets/adminlte3') ?>/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function (form) {
      console.log("Submitted!");
      form.submit();
    }
  });
  $('#frmLogin').validate({
    rules: {
      username: {
        required: true,
      },
      password: {
        required: true,
        minlength: 5
      },
    },
    messages: {
      username: {
        required: "Nhập tên đăng nhập của bạn"
      },
      password: {
        required: "Nhập mật khẩu của bạn",
        minlength: "Mật khẩu phải lớn hơn 5 ký tự"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.input-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>

</body>
</html>
