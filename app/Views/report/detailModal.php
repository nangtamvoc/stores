<div class="card border-top border-top-3 border-success">
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">

                <div class="row">
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="info-box bg-light">
                            <div class="info-box-content">
                                <span class="info-box-text text-center"><b>Max</b></span>
                                <span class="info-box-number text-center text-muted mb-0"><?=@$stores['MaxTotalPoint']?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="info-box bg-light">
                        <div class="info-box-content">
                            <span class="info-box-text text-center"><b>Tổng điểm</b></span>
                            <span class="info-box-number text-center text-muted mb-0"><?=@round($stores['TotalResualPoint'], 1)?></span>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="info-box bg-light">
                        <div class="info-box-content">
                            <span class="info-box-text text-center"><b>Điểm sau đánh giá:</b></span>
                            <span class="info-box-number text-center text-muted mb-0"><?=@round($stores['ResualPercentage'], 1)?>%<span>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="info-box bg-light">
                        <div class="info-box-content">
                            <span class="info-box-text text-center"><b>Xếp loại:</b></span>
                            <span class="info-box-number text-center text-muted mb-0"><?=@xepLoai(round($stores['ResualPercentage'], 1))?><span>
                        </div>
                        </div>
                    </div>
                </div>
                
                <?php 
                $array_new = [];
                if(!empty($stores['ShopFormula']['ShopFormulaDetail'])):
                    $emp = $manager = 0;
                    $array_new = $stores['ShopFormula']['ShopFormulaDetail'];
                    foreach($array_new as $k => $val):
                ?>
                    <div class="callout callout-danger">
                        <h5>Phần I: <?=$val['CateName']?></h5>
                    </div>
                    
                    <div class="row">
                        <div class="col-12">

                        <?php 
                            if(!empty($val['Items'])):
                            foreach($val['Items'] as $key=>$val2):
                        ?>
                                <div class="post">
                                    <div class="">
                                        <span class="username">
                                            <b><?=$key+1?>. <?=$val2['ItemName']?>: </b>
                                            <?=$val2['Note']?>
                                        </span>
                                    </div>
                                    <?php if(!empty($val2['ListValues'])) :?>
                                        <p>
                                            Sai format
                                        </p>
                                    <?php else: ?>
                                        <br>
                                        <p>
                                            <i><i class="fas fa-users mr-1"></i> Đánh giá nhân viên:</i>
                                            <?=$val2['EmpResult']?>
                                        </p>
                                        <p>
                                            <i><i class="fas fa-user mr-1"></i> Đánh giá quản lý:</i>
                                            <?=$val2['ManageResult']?>
                                        </p>

                                        <div class="row">
                                        <?php if(!empty( $val2['Images'] )) :
                                            $image = json_decode( $val2['Images'] );
                                            if(is_array($image)):
                                            foreach($image as $k => $v):
                                            ?>
                                            <div class="col-sm-2">
                                                <a href="<?= base_url().'/uploads/'.$v?>" data-toggle="lightbox" data-title="<?=$val2['ItemName']?>" data-gallery="gallery">
                                                    <img src="<?= base_url().'/uploads/'.$v?>" class="img-fluid mb-2" alt="white sample"/>
                                                </a>
                                            </div>
                                        <?php endforeach;?>
                                        <?php endif; endif;?>
                                        </div>
                                    <?php endif;?>
                                </div>

                        <?php   
                            endforeach;
                            endif;
                        ?>
                        </div>
                        
                    </div>

                <?php unset($array_new[0]);?>
                <?php break; endforeach;?>

                <div class="callout callout-danger">
                    <h5>Phần II: Các tiêu chí đánh giá</h5>
                </div>

                

                
                <?php
                        foreach($array_new as $k => $val):
                        ?>
                            <div class="">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="col-12">
                                            <h5><?= $k?>. <?=$val['CateName']?></h5>
                                        </div>
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col"><b>Điểm:</b> <?=$val['TotalPoint']?></h5></div>
                                                <div class="col"><b>Kết quả:</b> <?=round($val['TotalResualPoint'], 1)?></h5></div>
                                                <div class="col"><b>Max:</b> <?=$val['MaxTotalPoint']?></h5></div>
                                                <div class="col"><b>Trọng số:</b> <?=$val['CatePercentage']?>%</h5></div>
                                            </div>
                                            
                                        </div>
                                    </div>

                                    <div class="card-body">
                                            <div id="accordion<?=$val['CateCode']?>">
                                    <?php 
                                        if(!empty($val['Items'])):
                                            
                                        foreach($val['Items'] as $key=>$val2): 
                                    ?>
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title w-100">
                                                    <a class="d-block w-100"><i class="fas fa-hand-point-right"></i>
                                                    <?=$val2['ItemName']?>
                                                    
                                                    </a>
                                                </h4>
                                            </div>
                                            
                                            <div class="card-body">
                                                
                                                    <?php if(!empty($val2['ListValues'])) :?>
                                                        <?php
                                                            $arr = ['' => 'Không có dữ liệu'];
                                                            $listValues =  $val2['ListValues'];
                                                            $arrVal = json_decode($listValues, true);
                                                            foreach($arrVal as $key => $value) {
                                                                $arr[$value['value']] = $value['title'] .' ( '. $value['value'] . ' điểm )';
                                                            }
                                                        ?>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Điều kiện</label>
                                                                    <?= form_dropdown('point_'.$val2['MsfdId'], $arr, set_value('point_'.$val2['MsfdId'], round($val2['Point'], 0, PHP_ROUND_HALF_ODD)), 'class="form-control" disabled style="width: 100%;" ');?>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="col-md-6">
                                                                <ul class="list-group list-group-unbordered mb-3">
                                                                    <li class="list-group-item">
                                                                        <b>Kết quả</b> <a class="float-right"><?=round($val2['ResualPoint'], 1)?></a>
                                                                    </li>
                                                                    <li class="list-group-item">
                                                                        <b>Max</b> <a class="float-right"><?=$val2['MaxPoint']?></a>
                                                                    </li>
                                                                    <li class="list-group-item">
                                                                        <b>Trọng số</b> <a class="float-right"><?=$val2['PercentagePoint'].'%'?></a>
                                                                    </li>
                                                                </ul>
                                                                
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                    <?php if(!empty( $val2['Images'] )) :
                                                                        $image = json_decode( $val2['Images'] );
                                                                        if(is_array($image)):
                                                                        foreach($image as $k => $v):
                                                                        ?>
                                                                        <div class="col-sm-2">
                                                                            <a href="<?= base_url().'/uploads/'.$v?>" data-toggle="lightbox" data-title="<?=$val2['ItemName']?>" data-gallery="gallery">
                                                                                <img src="<?= base_url().'/uploads/'.$v?>" class="img-fluid mb-2" alt="white sample"/>
                                                                            </a>
                                                                        </div>
                                                                    <?php endforeach;?>
                                                                    <?php endif; endif;?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>   
                                                    <?php else:?>
                                                        
                                                    <?php endif;?>
                                            </div>
                                           
                                        </div>
                                        
                                    <?php 
                                        endforeach;
                                        endif;
                                    ?>
                                    </div>
                                    </div>
                                    
                                </div>
                            </div>
                            
                            
                        <?php endforeach;?>




                <?php endif;?>



            </div>
            <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
                <h3 class="text-primary"><i class="fas fa-paint-brush"></i> <?=$stores['ShopName']?></h3>
                <p class="text-muted"><?=$stores['Note']?></p>
                <br>
                <div class="text-muted">
                    <p class="text-md">Địa chỉ:
                        <b class="d-block"><?=$stores['Address'].' - '.$stores['WardName'].' - '.$stores['DistrictName'].' - '.$stores['ProvinceName']?></b>
                    </p>
                    <p class="text-md">Phụ trách:
                        <b class="d-block"><?=$stores['Assign']?></b>
                    </p>
                    <p class="text-md">Trạng thái:
                        <b class="d-block"><?=getStatusStores($stores['Status'])?></b>
                    </p>
                    <p class="text-md">Ngày đánh giá:
                        <b class="d-block"> <?=formatDateTime($stores['ShopFormula']['ReviewDate'])?></b>
                    </p>
                </div>

                <!-- <h5 class="mt-5 text-muted">Project files</h5>
                <ul class="list-unstyled">
                    <li>
                        <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-word"></i> Functional-requirements.docx</a>
                    </li>
                    <li>
                        <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-pdf"></i> UAT.pdf</a>
                    </li>
                    <li>
                        <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-envelope"></i> Email-from-flatbal.mln</a>
                    </li>
                    <li>
                        <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-image "></i> Logo.png</a>
                    </li>
                    <li>
                        <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-word"></i> Contract-10_12_2014.docx</a>
                    </li>
                </ul>
                <div class="text-center mt-5 mb-3">
                    <a href="#" class="btn btn-sm btn-primary">Add files</a>
                    <a href="#" class="btn btn-sm btn-warning">Report contact</a>
                </div> -->

            </div>
        </div>
    </div>
</div>