<?= $this->extend('layouts/master') ?>

<?= $this->section('head') ?>
<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- daterange picker -->
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/daterangepicker/daterangepicker.css">
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/datatables-fixedcolumns/css/fixedColumns.bootstrap4.min.css">
<!-- Ekko Lightbox -->
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/ekko-lightbox/ekko-lightbox.css">
<!-- Jquery Confirm -->
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/jquery-confirm/jquery-confirm.css">

<link rel="stylesheet" href="<?= base_url('assets') ?>/css/main.css">
<style type="text/css">
    th, td{white-space: nowrap;}
    .post{color:black !important}
</style>
<?= $this->endSection() ?>

<?= $this->section('foot') ?>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/select2/js/select2.full.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url('assets/adminlte3') ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/jszip/jszip.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/datatables-buttons/js/buttons.print.min.js"></script>

<script src="<?= base_url('assets/adminlte3') ?>/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/datatables-fixedcolumns/js/dataTables.fixedColumns.min.js"></script>
<script src="<?= base_url('assets')?>/js/custom-datatablejs.js"></script>

<!-- Ekko Lightbox -->
<script src="<?= base_url('assets/adminlte3') ?>/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>

<!-- Jquery Moment -->
<script src="<?= base_url('assets/adminlte3') ?>/plugins/moment/moment.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/moment/locale/vi.js"></script>
<!-- date-range-picker -->
<script src="<?= base_url('assets/adminlte3') ?>/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Jquery Confirm -->
<script src="<?= base_url('assets/adminlte3') ?>/plugins/jquery-confirm/jquery-confirm.js"></script>

<!-- page script -->
<script>
$(function () {
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox({
            alwaysShowClose: true
        });
    });
    //Initialize Select2 Elements
    $('.select2').select2()
    // Set date default
    var today = new Date();
    today.setDate(today.getDate() - 10);
    //Date range picker
    $('#reservation').daterangepicker({
        locale: {
            format: 'DD/MM/YYYY',
            separator: " - ",
            applyLabel: "Đồng ý",
            "cancelLabel": "Huỷ",
            "fromLabel": "Từ",
            "toLabel": "Đến",
            "customRangeLabel": "Custom",
        },
        startDate: today,
        endDate: new Date()
        
    })
    function pstatus(x) {
        if (x == 0) {
            return '<span class="font-weight-bolder text-muted">Chưa kích hoạt</span>';
        }else if(x == 1) {
            return '<span class="font-weight-bolder text-info">Kích hoạt</span>';
        }else if(x == 3) {
            return '<span class="font-weight-bolder text-warning">Không đạt</span>';
        }else if(x == 5) {
            return '<span class="font-weight-bolder text-primary">Đạt</span>';
        }else if(x == 7) {
            return '<span class="font-weight-bolder text-danger">Không đạt</span>';
        }else if(x == 9) {
            return '<span class="font-weight-bolder text-success">Đạt</span>';
        }
    }
    let getStoresApi = '<?= base_url("report/get_stores") ?>';
    var currentFilter = {
        <?= csrf_token() ?>: '<?= csrf_hash() ?>',
        reservation: '<?=$reservation?>',
        province: 'ALL',
        district: 'ALL',
        ward: 'ALL',
        assign: 'ALL',
    }

    function getParams() {
        let params = '<?= csrf_token() ?>=' + currentFilter.<?= csrf_token() ?>
            + '&reservation=' + currentFilter.reservation
            + '&assign=' + currentFilter.assign
            + '&province=' + currentFilter.province
            + '&district=' + currentFilter.district
            + '&ward=' + currentFilter.ward;
        return params;
    }

    $('#btnSearch').click(function () {

        currentFilter.<?= csrf_token() ?> = $('.txt_csrfname').val();
        currentFilter.reservation = $('#reservation').val()
        currentFilter.province = $('#selectProvince').val()
        currentFilter.district = $('#selectDistrict').val()
        currentFilter.ward = $('#selectWard').val()
        currentFilter.assign = $('#assign').val()

        reportTable.search('');
        reportTable.clearPipeline();
        reportTable.ajax.url( getStoresApi + '?' + getParams()).load();
    });
    
    let reportTable = $('#reportTable').on('preXhr.dt', function (e, settings, data) {
        $('.content').LoadingOverlay("show");
    }).on('xhr.dt', function (e, settings, json, xhr) {
        $('.content').LoadingOverlay("hide");
    }).DataTable({
        dom: 'Blrftip',
        scrollY: '60vh',
        lengthMenu: [[100, 200, 300, -1], [100, 200, 300, 'Tất cả']],
        scrollX: true,
        scrollCollapse: true,
        processing: false,
        // serverSide: true,
        searching: true,
        search: {
            "caseInsensitive": true,
        },
        fixedColumns: {
            leftColumns: 2
        },
        oLanguage: Datatable_Language,
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fas fa-columns"></i> Ẩn/Hiện cột',
                className: 'btn btn-sm btn-warning',
                columns: ':not(.noVis)'
            },
            {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel"></i> Xuất Excel',
                className: 'btn btn-sm btn-warning',
                footer: true,
                exportOptions: { columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15] },
                init: function(api, node, config) {
                    $(node).removeClass('btn-default')
                }
            }
        ],
        columnDefs: [
            {
                targets: 0,
                className: 'noVis'
            }
        ],
        ajax: getStores(),
        columns: [
            { title: "STT", className: "text-center", searchable: false, orderable: false, data: null},
            { title: "Phụ trách", className: "text-right", data: 'Assign'},
            { title: "Mã", data: 'ShopCode', className: "text-right", visible: false},
            { title: "Tên mặt bằng", data: 'ShopName'},
            { title: "Ngày tạo", width: 70, data: 'CreateDate', render: formatDate},
            { title: "Trạng thái", data: 'Status', render: pstatus},
            { title: "Số điện thoại", data: 'Tel'},
            { title: "Địa chỉ", data: 'Address'},
            { title: "Phường/Xã", data: 'WardName'},
            { title: "Quận/Huyện", data: 'DistrictName'},
            { title: "Tỉnh/Thành phố", data: 'ProvinceName', visible: false},
            { title: "Email", data: 'Email', visible: false},
            { title: "Ngày mở", width: 70, data: 'OpenDate', render: formatDate, visible: false},
            { title: "Max", data: 'MaxTotalPoint'},
            { title: "Tổng điểm", data: 'TotalResualPoint' , render : function ( data, type, row ) {
                    return Math.round(parseFloat(row.TotalResualPoint) * 100) / 100;
                }
            },
            { title: "Số điểm sau đánh giá", data: 'ResualPercentage', render: function ( data, type, row ) {
                    return Math.round(parseFloat(row.ResualPercentage) * 100) / 100;
                }
            },

        ],
        order: [[ 2, "asc" ]],
        initComplete: function (settings, json) {
            $('#reportTable_filter input').bind('keyup', function (e) {
                console.log(e.keyCode);
                if (e.keyCode == 13) {
                    reportTable.search(this.value).draw();
                }
            });
            $('.txt_csrfname').val(json.csrf_nutistore_name);
        },
    });

    reportTable.on( 'order.dt search.dt', function () {
        reportTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    // reportTable.on('user-select', function (e, dt, type, cell, originalEvent) {
    //     let row = dt.row(cell.index().row).node();
    //     $(row).removeClass('hover');
    //     console.log(row)
    //     if ($(row).hasClass('selected')) {
    //         e.preventDefault();
    //     }
    // });

    function getStores() {
        return $.fn.dataTable.pipeline({
            url: getStoresApi,
            pages: 5, // number of pages to cache,
            method: 'get',
            data: currentFilter
        });
    }

    reportTable.on('dblclick', 'tr', function (e, dt, type, cell, originalEvent) {
        if (!reportTable.rows().data().length) return
        let rowData = reportTable.row(this).data()
        $.ajax({
            url: '<?=site_url('report/get_stores_detail');?>',
            type: 'post',
            data: { id: rowData.MsId },
            beforeSend: function (jqXHR, settings) {
            },
            success: function (newHTML, textStatus, jqXHR) {
                $('#detailStoreModal').on('show.bs.modal', function (event) {
                    let modal = $(this);
                    modal.find('.modal-title').text('Thông tin chi tiết: ' + rowData.ShopName)
                    modal.find('.modal-body').html(newHTML);
                });
                $('#detailStoreModal').modal('show');
            },
            complete: function (jqXHR, textStatus) {
            }
        });

    });

});

</script>

<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="row">
    <div class="col-12">

        <div class="card border-top border-top-3 border-success">
            <div class="card-header">
                <a class="btn btn-success btn-sm" href="<?= base_url("stores/add")?>" title="Thêm mới mặt bằng" ><i class="fas fa-plus"></i></i> Thêm mới
                </a>
                <a href="#" class="btn btn-warning btn-sm toggle_form float-right"><i class="far fa-eye-slash"></i> Ẩn/Hiện Tìm kiếm</a>
            </div>
            <div class="card-body">
                <div id="form" class="panel panel-warning mb-3">
                <input type="hidden" class="txt_csrfname" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>" />
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label class="control-label" for="code">Ngày tạo:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control form-control-sm float-right" id="reservation">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label class="control-label" for="provinceCode">Tỉnh/Thành</label>
                                    <?= form_dropdown('provinceCode', @$provinceList, set_value('provinceCode', @$provinceCode), 'class="form-control select2 form-control-sm" id="selectProvince" style="width:100%;"'); ?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label class="control-label" for="districtCode">Quận/huyện</label>
                                    <?= form_dropdown('districtCode', @$arrayDistrict, set_value('provinceCode', @$districtCode), 'class="form-control select2 form-control-sm" id="selectDistrict" style="width:100%;"'); ?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label class="control-label" for="wardCode">Phường/Xã</label>
                                    <?= form_dropdown('wardCode', @$arrayWard, set_value('provinceCode', @$wardCode), 'class="form-control select2 form-control-sm" id="selectWard" style="width:100%;"'); ?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label class="control-label" for="assign">Người khảo sát</label>
                                    <?= form_dropdown('assign', @$emps, set_value('assign', @$assign), 'class="form-control select2 form-control-sm" id="assign" style="width:100%;"'); ?>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <button id="btnSearch" class="btn btn btn-success btn-sm"><i class="fas fa-search"></i> Tìm kiếm</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="callout callout-warning">
                    <h5><i class="fas fa-info"></i> HD:</h5>
                    Nhấp chuột 2 lần vào dòng trong bảng để xem chi tiết báo cáo cho mặt bằng.
                </div>
                <div class="table-responsive">
                    <table id="reportTable" class="table table-striped table-bordered table-condensed table-hover dataTable no-footer nowrap">
                        
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<!-- Modal -->
<div class="modal fade" id="detailStoreModal" tabindex="-1" role="dialog" data-bs-backdrop="static" aria-labelledby="detailStoreModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Default Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>