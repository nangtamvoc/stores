<?= $this->extend('layouts/master') ?>

<?= $this->section('head') ?>
<!-- bootstrap-datetimepicker -->
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css">

<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

<!-- Jquery Confirm -->
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/jquery-confirm/jquery-confirm.css">

<link rel="stylesheet" href="<?= base_url('assets') ?>/css/main.css">
<?= $this->endSection() ?>

<?= $this->section('foot') ?>
<!-- Jquery Moment -->
<script src="<?= base_url('assets/adminlte3') ?>/plugins/moment/moment.min.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="<?= base_url('assets/adminlte3') ?>/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<!-- Select2 -->
<script src="<?= base_url('assets/adminlte3') ?>/plugins/select2/js/select2.full.min.js"></script>
<!-- jquery-validation -->
<script src="<?= base_url('assets/adminlte3') ?>/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/jquery-validation/additional-methods.min.js"></script>
<!-- Jquery Confirm -->
<script src="<?= base_url('assets/adminlte3') ?>/plugins/jquery-confirm/jquery-confirm.js"></script>

<script>
$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
    $('.datepicker').datetimepicker({
        format: 'DD-MM-YYYY',
        sideBySide: false,
    });

    jQuery.validator.addMethod('valid_email', function (value, element) {
        var regex = /^[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{1,5}$/;
        return this.optional(element) || regex.test(value);
    }, 'Vui lòng nhập đúng định dạng email');
    jQuery.validator.addMethod("customphone", function (value, element) {
        return this.optional(element) || /((09|03|07|08|05)+([0-9]{8})\b)/.test(value);
    }, 'Vui lòng nhập đúng định dạng số điện thoại');

    var validate_form = $('#form-store').validate({
        rules: {
            shopcode: {
                required: true,
            },
            shopname: {
                required: true,
            },
            fullName: {
                required: true,
            },
            email: {
                required: true,
                // email: true,
                valid_email: true
            },
            tel: {
                number: true,
                customphone: true
            },
            
        },
        messages: {
            shopcode: {
                required: "Vui lòng điền Mã mặt bằng"
            },
            shopname: {
                required: "Vui lòng điền Tên mặt bằng"
            },
            email: {
                required: "Vui lòng nhập địa chỉ email",
            },
            tel: {
                number: 'Số điện thoại phải là số'
            },
            address: {
                required: "Vui lòng nhập địa chỉ"
            },
            provincecode: {
                required: "Vui lòng chọn Tỉnh/thành phố"
            },
            districtcode: {
                required: "Vui lòng chọn Quận/Huyện"
            },
            wardcode: {
                required: "Vui lòng chọn Phường/Xã"
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function (form) {
            form.submit();
            // var paramArr = $(form).serializeArray();
            // console.log(paramArr)
            // $.post( $('#form-user').attr('action'), $.param(paramArr), [], 'json')
            //         .done(function (e) {
            //             console.log(e);
            //             if(e.success){
            //                 jqueryAlert('Thông báo', e.message, 'green');
            //                 $('#createStoreModal').modal('hide');
            //                 userTable.ajax.reload();
            //             }else{
            //                 jqueryAlert('Thông báo', e.message, 'red');
            //             }
            //         })
            //         .fail(function (xhr, textStatus, errorThrown) {
            //             console.log(xhr.responseText);
            //         });
        }
    });
});

</script>

<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="card card-default border-top border-top-3 border-success">
    <div class="card-header">
        <h3 class="card-title">
            Vui lòng nhập các thông tin
        </h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <?= form_open_multipart("stores/submitStore", 'id="form-store"');?>
        <?php
            $data = [
                'type'  => 'hidden',
                'name'  => 'type',
                'id'    => 'type',
                'value' => 'add',
                'class' => 'type',
            ];
            echo form_input($data);
        ?>
        <input type="hidden" id="lon" name="lon" value="0">
        <input type="hidden" id="lat" name="lat" value="0">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="shopcode">Mã mặt bằng<span class="text-danger">(*)</span></label>
                    <?= form_input('shopcode', set_value('shopcode'), 'class="form-control" id="shopcode" required');?>
                </div>
                <div class="form-group">
                    <label for="shopname">Tên mặt bằng <span class="text-danger">(*)</span></label>
                    <?= form_input('shopname', set_value('shopname'), 'class="form-control" id="shopname" required');?>
                </div>
                <div class="form-group">
                    <label for="tel">Số điện thoại</label>
                    <?= form_input('tel', set_value('tel'), 'class="form-control" id="tel"');?>
                </div>
                <div class="form-group">
                    <label for="email">Email <span class="text-danger">(*)</span></label>
                    <?= form_input('email', set_value('email'), 'class="form-control" id="email" required');?>
                </div>
                <div class="form-group">
                    <label for="note">Ghi chú</label>
                    <?php 
                        $data = array(
                            'name'        => 'note',
                            'id'          => 'note',
                            'value'       => set_value('note'),
                            'rows'        => '5',
                            'cols'        => '5',
                            'class'       => 'form-control'
                        );
                        echo form_textarea($data);
                    ?>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-md-6">
                <?php if( session()->get('user')['User']['RoleId'] == 2 ):?>
                <div class="form-group">
                    <label for="assign">Phụ trách</label>
                    <?= form_dropdown('assign', $dataEmp, set_value('assign'), 'class="form-control select2" id="assign"'); ?>
                </div>
                <?php else:?>
                    <?php
                        $data = [
                            'type'  => 'hidden',
                            'name'  => 'assign',
                            'id'    => 'assign',
                            'value' => session()->get('user')['User']['UserName']
                        ];
                        echo form_input($data);
                    ?>
                <?php endif?>
                <div class="form-group">
                    <label for="opendate">Ngày khai trương dự tính</label>
                    <?= form_input('opendate', set_value('opendate'), 'class="form-control datepicker col-sm-6"'); ?>
                </div>
                <div class="form-group">
                    <label for="address">Địa chỉ <span class="text-danger">(*)</span></label>
                    <?= form_input('address', set_value('address'), 'class="form-control" id="address" required');?>
                </div>
                <div class="form-group">
                    <label for="provincecode">Tỉnh thành phố <span class="text-danger">(*)</span></label>
                    <?= form_dropdown('provincecode', $provinceList, set_value('provincecode'), 'class="form-control select2" id="selectProvince" required'); ?>
                </div>
                <div class="form-group">
                    <label for="districtcode">Quận/Huyện <span class="text-danger">(*)</span></label>
                    <?= form_dropdown('districtcode', [], set_value('districtcode'), 'class="form-control select2" id="selectDistrict" required'); ?>
                </div>
                <div class="form-group">
                    <label for="wardcode">Phường/Xã <span class="text-danger">(*)</span></label>
                    <?= form_dropdown('wardcode', [], set_value('wardcode'), 'class="form-control select2" id="selectWard" required'); ?>
                </div>

            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-12">
                <?= form_submit('add_shop', 'Lưu', 'class="btn btn-success"'); ?>
                <a class="btn btn-default" href="<?= base_url("stores") ?>">Quay lại</a>
            </div>
        </div>
        <!-- /.row -->
    <?= form_close();?>
    <!-- /.row -->
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
    <span class="text text-danger">(*) bắt buộc, không được bỏ trống</span>
    </div>
</div>
<!-- /.card -->
<?= $this->endSection() ?>