<?= $this->extend('layouts/master') ?>

<?= $this->section('head') ?>
<!-- Tempusdominus Bbootstrap 4 -->
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Bootstrap4 Duallistbox -->
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
<!-- Jquery Confirm -->
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/jquery-confirm/jquery-confirm.css">
<!-- dropzone -->
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/dropzone/dropzone.min.css">
<link rel="stylesheet" href="<?= base_url('assets') ?>/css/main.css">
<link rel="stylesheet" href="<?= base_url('assets') ?>/css/zoom.css">
<?= $this->endSection() ?>

<?= $this->section('foot') ?>

<!-- Select2 -->
<script src="<?= base_url('assets/adminlte3') ?>/plugins/select2/js/select2.full.min.js"></script>
<!-- jquery-validation -->
<script src="<?= base_url('assets/adminlte3') ?>/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/jquery-validation/additional-methods.min.js"></script>
<!-- Jquery Confirm -->
<script src="<?= base_url('assets/adminlte3') ?>/plugins/jquery-confirm/jquery-confirm.js"></script>

<script src="<?= base_url('assets') ?>/js/zoom.js"></script>

<script>
$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    var validate_form = $('#form-review').validate({
        submitHandler: function (form) {
            form.submit();

            // var paramArr = $(form).serializeArray();
            // console.log(paramArr)
            // $.post( $('#form-review').attr('action'), $.param(paramArr), [], 'json')
            //     .done(function (e) {
            //         if(e.success){
            //             $.alert({
            //                 animation: 'none',
            //                 title: 'Đánh giá mặt bằng thành công!',
            //                 content: '',
            //                 type: 'green',
            //                 containerFluid: true,
            //                 buttons: {
            //                     close: {
            //                         text: 'Đóng',
            //                         btnClass: 'btn-secondary',
            //                         action: function () {
            //                             window.location.href = '<?= base_url("stores")?>'
            //                         }
            //                     }
            //                 }
            //             });
            //         }
            //     })
            //     .fail(function (xhr, textStatus, errorThrown) {
            //         console.log(xhr.responseText);
            //     });
        }
    });

    /*
    // DropzoneJS Demo Code Start
    Dropzone.autoDiscover = false

    // Get the template HTML and remove it from the doument he template HTML and remove it from the doument
    var previewNode = document.querySelector("#template")
    previewNode.id = ""
    var previewTemplate = previewNode.parentNode.innerHTML
    previewNode.parentNode.removeChild(previewNode)

    var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
        url: "/target-url", // Set the url
        thumbnailWidth: 80,
        thumbnailHeight: 80,
        parallelUploads: 20,
        previewTemplate: previewTemplate,
        autoQueue: false, // Make sure the files aren't queued until manually added
        previewsContainer: "#previews", // Define the container to display the previews
        clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
    })

    myDropzone.on("addedfile", function(file) {
        // Hookup the start button
        file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file) }
    })

    // Update the total progress bar
    myDropzone.on("totaluploadprogress", function(progress) {
        document.querySelector("#total-progress .progress-bar").style.width = progress + "%"
    })

    myDropzone.on("sending", function(file) {
        // Show the total progress bar when upload starts
        document.querySelector("#total-progress").style.opacity = "1"
        // And disable the start button
        file.previewElement.querySelector(".start").setAttribute("disabled", "disabled")
    })

    // Hide the total progress bar when nothing's uploading anymore
    myDropzone.on("queuecomplete", function(progress) {
        document.querySelector("#total-progress").style.opacity = "0"
    })

    // Setup the buttons for all transfers
    // The "add files" button doesn't need to be setup because the config
    // `clickable` has already been specified.
    document.querySelector("#actions .start").onclick = function() {
        myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED))
    }
    document.querySelector("#actions .cancel").onclick = function() {
        myDropzone.removeAllFiles(true)
    }
  
   */ 

// $("#id_dropzone").dropzone({
//     maxFiles: 2000,
//     url: "/ajax_file_upload_handler/",
//     success: function (file, response) {
//         console.log(response);
//     }
// });

});

</script>

<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="card card-default border-top border-top-3 border-success">
    <div class="card-header">
        <h3 class="card-title">
            Thông tin chung mặt bằng
        </h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
        </div>
    </div>
    <!-- /.card-header -->
    <?= form_open_multipart("stores/submitStore", 'id="form-review"');?>
    <?php
        $data = [
            'type'  => 'hidden',
            'name'  => 'type',
            'id'    => 'type',
            'value' => 'review',
            'class' => 'type',
        ];
        echo form_input($data);
        $data = [
            'type'  => 'hidden',
            'name'  => 'MsId',
            'id'    => 'shopid',
            'value' => $stores['MsId']
        ];
        echo form_input($data);
    ?>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <strong><i class="fas fa-map-marker-alt mr-3"></i> Địa chỉ:</strong>
                <a class="text-info font-weight-normal"><?= $stores['Address'].', '. $stores['WardName'] .', '. $stores['DistrictName'] . ', '. $stores['ProvinceName']?></a>
                <br>
                <strong><i class="fas fa-user mr-3"></i> Người phụ trách:</strong>
                <a class="text-info"><?= $stores['Assign']?></a>
                <br>
                <strong><i class="fas fa-bell mr-3"></i> Trạng thái:</strong>
                <a class="text-info"><?= getStatusStores($stores['Status'])?></a>
                <br>
                <strong><i class="fas fa-calendar mr-3"></i> Ngày đánh giá:</strong>
                <a class="text-info">
                    <?= formatDateTime($stores['ShopFormula']['ReviewDate'])?>
                </a>
            </div>
            <div class="col-md-6">
                
                <strong><i class="fas fa-drafting-compass mr-3"></i>Max:</strong>
                <a class="text-info"><?=@$stores['MaxTotalPoint']?></a>
                <br>
                <strong><i class="fas fa-pencil-ruler mr-3"></i>Tổng điểm:</strong>
                <a class="text-info"><?=@round($stores['TotalResualPoint'], 1)?></a>
                <br>
                <strong><i class="fas fa-bezier-curve mr-3"></i>Xếp loại:</strong>
                <a class="text-info"><?=@xepLoai(round($stores['ResualPercentage'], 1))?></a>
                <br>
                <strong><i class="fas fa-highlighter mr-3"></i>Điểm số sau đánh giá: </strong>
                <a class="text-info"><?=@round($stores['ResualPercentage'], 1)?>%</a>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
            <?php
                $array_new = [];
                if(!empty($stores['ShopFormula']['ShopFormulaDetail'])):
                    $emp = $manager = 0;
                    $array_new = $stores['ShopFormula']['ShopFormulaDetail'];
                    foreach($array_new as $k => $val):
                    ?>
                    <div class="callout callout-danger">
                        <h5>Phần I: <?=$val['CateName']?></h5>
                    </div>
                    <div id="accordion1">
                    <?php 
                        if(!empty($val['Items'])):
                        $maxImage = 5;
                        foreach($val['Items'] as $key=>$val2):
                            if(session()->has('imagenew_'.$val2['MsfdId'])){
                                $arrImage = session()->get('imagenew_'.$val2['MsfdId']);
                                foreach ($arrImage as $key => $value) {
                                    @unlink(ROOTPATH .'public/uploads/'.$value);
                                }
                                session()->remove('imagenew_'.$val2['MsfdId']);
                            }
                            @session()->remove('image_del_'.$val2['MsfdId']);
                            @session()->remove('image_current_'.$val2['MsfdId']);
                    ?>
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title w-100">
                                    <a class="d-block w-100" data-toggle="collapse" href="#collapse<?=$val2['MsfdId']?>">
                                        <?=$val2['ItemName']?>
                                        <?php ($val2['ItemName'] == 'Pháp lý')?$maxImage = 15 : $maxImage = 5;?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse<?=$val2['MsfdId']?>" class="collapse <?=($key==0)?'show':''?>" data-parent="#accordion1">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php if(!empty($val2['ListValues'])) :?>
                                                <?php
                                                    $arr = ['' => 'Vui lòng chọn'];
                                                    $listValues =  $val2['ListValues'];
                                                    $arrVal = json_decode($listValues, true);
                                                    foreach($arrVal as $key => $value) {
                                                        $arr[$value['value']] = $value['title'] .' ( '. $value['value'] . ' điểm )';
                                                    }
                                                ?>
                                                <!-- <div class="form-group col-md-12">
                                                    <?= form_dropdown('point_'.$val2['MsfdId'], $arr, set_value('point_'.$val2['MsfdId'], round($val2['Point'], 0, PHP_ROUND_HALF_ODD)), 'class="form-control select2" id="" ');?>
                                                </div>
                                                
                                                <div class="form-group formuploadimg_post col-md-6">
                                                    <label>Hình ảnh</label>
                                                    <input maximage="<?=$maxImage?>" msfdid="<?=$val2['MsfdId']?>" shopid="<?=$stores['MsId']?>" countimg='<?=@count(json_decode($val2['Images']))?>' type="file" name="files" id="files_<?=$val2['MsfdId']?>" multiple class="form-control-file files">
                                                    <p style="color: red;">Số lượng tối đa là <?=$maxImage?> tấm ảnh</p>
                                                </div>
                                                <div class="form-group">
                                                    <div style="display: none;" class="alert alert-danger" id="message_<?=$val2['MsfdId']?>" role="alert">
                                                        This is a danger alert—check it out!
                                                    </div>
                                                    <div class="row" id="uploaded_images_<?=$val2['MsfdId']?>">
                                                        <?php if( !empty($stores['SHOP_IMAGEs'][0]['IMAGE']) && $stores['SHOP_IMAGEs'][0]['IMAGE'] != 'null') :
                                                            $image = json_decode($stores['SHOP_IMAGEs'][0]['IMAGE']);
                                                            if(is_array($image)):
                                                            $dataImage = array(
                                                                'image_current' => $image
                                                            );
                                                            $this->session->set_userdata($dataImage);
                                                            foreach($image as $k => $v):
                                                            ?>
                                                            <div class="col-sm-3">
                                                                <a shopid="<?=$stores['ROW_ID']?>" onclick="del_image(this)" name_image="<?=$v?>" class="del_image" title="Xóa">x</a>
                                                                <img src="<?= site_url().URL_SHOPS.$v?>" class="img-responsive img-thumbnail" data-action="zoom">
                                                            </div>
                                                        <?php endforeach;?>
                                                        <?php endif; endif;?>
                                                    </div>
                                                </div> -->
                                                
                                            <?php else:?>
                                                    <div class="form-group col-md-6">
                                                        <?php 
                                                            $data = array(
                                                                'name'        => 'note_'.$val2['MsfdId'],
                                                                'id'          => 'note_'.$val2['MsfdId'],
                                                                'value'       => set_value('note', $val2['Note']),
                                                                'rows'        => '2',
                                                                'cols'        => '4',
                                                                'class'       => 'form-control'
                                                            );
                                                            echo form_textarea($data);
                                                        ?>
                                                    </div>
                                                    <?php
                                                    // if($val2['EmpResult'] == 'Đạt'){
                                                    //     $emp++;
                                                    // }
                                                    // if($val2['ManageResult'] == 'Đạt'){
                                                    //     $manager++;
                                                    // }
                                                    if(session()->get('user')['User']['RoleId'] == 1){
                                                        $disabled = '';
                                                        $textLabel = 'Đánh giá';
                                                    } else {
                                                        $disabled = 'disabled';
                                                        $textLabel = 'Đánh giá của nhân viên';
                                                    }
                                                    ?>
                                                    <div class="form-group col-md-6">
                                                        <?php 
                                                            $empResult = [
                                                                ''      => 'Vui lòng chọn',
                                                                'Đạt'   => 'Đạt',
                                                                'Không đạt' => 'Không đạt'
                                                            ];
                                                        ?>
                                                        <label><?=$textLabel?></label>
                                                        <?= form_dropdown('empResult_'.$val2['MsfdId'], $empResult, set_value('empResult_'.$val2['MsfdId'], $val2['EmpResult']), 'class="form-control" '. $disabled.' ');?>
                                                    </div>
                                                    <?php if(session()->get('user')['User']['RoleId'] == 2) :?>
                                                        <div class="form-group col-md-6">
                                                            <?php 
                                                                $manageResult = [
                                                                    ''      => 'Vui lòng chọn',
                                                                    'Đạt'   => 'Đạt',
                                                                    'Không đạt' => 'Không đạt'
                                                                ];
                                                            ?>
                                                            <label>Đánh giá</label>
                                                            <?= form_dropdown('manageResult_'.$val2['MsfdId'], $manageResult, set_value('manageResult_'.$val2['MsfdId'], (empty($val2['ManageResult'])?$val2['EmpResult']:$val2['ManageResult']) ), 'class="form-control" ');?>
                                                        </div>
                                                    <?php endif;?>


                                                    <div class="form-group formuploadimg_post col-md-6">
                                                        <label>Hình ảnh</label>
                                                        <input maximage="<?=$maxImage?>" msfdid="<?=$val2['MsfdId']?>" shopid="<?=$stores['MsId']?>" countimg='<?=@count(json_decode($val2['Images']))?>' type="file" name="files[]" id="files_<?=$val2['MsfdId']?>" multiple class="form-control-file files">
                                                        <p style="color: red;">Số lượng tối đa là <?=$maxImage?> tấm ảnh</p>
                                                    </div>


                                                    <div class="form-group">
                                                        <div style="display: none;" class="alert alert-danger" id="message_<?=$val2['MsfdId']?>" role="alert">
                                                            This is a danger alert—check it out!
                                                        </div>
                                                        <div class="row" id="uploaded_images_<?=$val2['MsfdId']?>">
                                                        <?php if(!empty( $val2['Images'] )) :
                                                            $image = json_decode( $val2['Images'] );
                                                            if(is_array($image)):
                                                            $dataImage = array(
                                                                'image_current_'.$val2['MsfdId'] => $image
                                                            );
                                                            session()->set($dataImage);
                                                            foreach($image as $k => $v):
                                                            ?>
                                                            <div class="col-sm-2">
                                                                <a msfdid="<?=$val2['MsfdId']?>" shopid="<?=$stores['MsId']?>" onclick="del_image(this)" name_image="<?=$v?>" class="del_image" title="Xóa" class="btn btn-default"><i class="fas fa-trash-alt"></i> Xoá</a>
                                                                <img src="<?= base_url().'/uploads/'.$v?>" class="img-responsive img-thumbnail" data-action="zoom">
                                                            </div>
                                                        <?php endforeach;?>
                                                        <?php endif; endif;?>
                                                        </div>
                                                    </div>


                                            <?php endif;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php   
                        endforeach;
                        endif;
                        unset($array_new[0]);
                    break;
                    ?>
                    </div>
                <?php endforeach;?>
                
                <br>
                        <div class="callout callout-danger">
                            <h5>Phần II: Các tiêu chí đánh giá</h5>
                        </div>
                        <?php
                        foreach($array_new as $k => $val):
                        ?>
                            <div class="">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="col-12">
                                            <h5 style="border-bottom: 3px solid #bd2130;width: 50%;border-radius: 3px;"><?= $k?>. <?=$val['CateName']?></h5>
                                        </div>
                                        
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col"><b>Điểm:</b> <?=$val['TotalPoint']?></h5></div>
                                                <div class="col"><b>Kết quả:</b> <?=round($val['TotalResualPoint'], 1)?></h5></div>
                                                <div class="col"><b>Max:</b> <?=$val['MaxTotalPoint']?></h5></div>
                                                <div class="col"><b>Trọng số:</b> <?=$val['CatePercentage']?>%</h5></div>
                                            </div>
                                            
                                        </div>
                                    </div>

                                    <div class="card-body">
                                            <div id="accordion<?=$val['CateCode']?>">
                                    <?php 
                                        if(!empty($val['Items'])):
                                            $maxImage = 5;
                                        foreach($val['Items'] as $key=>$val2):

                                            if(session()->has('imagenew_'.$val2['MsfdId'])){
                                                $arrImage = session()->get('imagenew_'.$val2['MsfdId']);
                                                foreach ($arrImage as $key => $value) {
                                                    @unlink(ROOTPATH .'public/uploads/'.$value);
                                                }
                                                session()->remove('imagenew_'.$val2['MsfdId']);
                                            }
                                            @session()->remove('image_del_'.$val2['MsfdId']);
                                            @session()->remove('image_current_'.$val2['MsfdId']);  
                                    ?>
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title w-100">
                                                    <a class="d-block w-100" data-toggle="collapse" href="#collapse<?=$val2['MsfdId']?>"><i class="fas fa-hand-point-right"></i>
                                                    <?=$val2['ItemName']?>
                                                    <?php ($val2['ItemName'] == 'Pháp lý')?$maxImage = 15 : $maxImage = 5;?>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse<?=$val2['MsfdId']?>" class="collapse <?=($key==0)?'show':''?>" data-parent="#accordion<?=$val['CateCode']?>">
                                            <div class="card-body">
                                                
                                                    <?php if(!empty($val2['ListValues'])) :?>
                                                        <?php
                                                            $arr = ['' => 'Vui lòng chọn'];
                                                            $listValues =  $val2['ListValues'];
                                                            $arrVal = json_decode($listValues, true);
                                                            foreach($arrVal as $key => $value) {
                                                                $arr[$value['value']] = $value['title'] .' ( '. $value['value'] . ' điểm )';
                                                            }
                                                        ?>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Điều kiện</label>
                                                                    <?= form_dropdown('point_'.$val2['MsfdId'], $arr, set_value('point_'.$val2['MsfdId'], round($val2['Point'], 0, PHP_ROUND_HALF_ODD)), 'class="form-control select2" style="width: 100%;" ');?>
                                                                </div>

                                                                <div class="form-group formuploadimg_post col-md-6">
                                                                    <label>Hình ảnh</label>
                                                                    <input maximage="<?=$maxImage?>" msfdid="<?=$val2['MsfdId']?>" shopid="<?=$stores['MsId']?>" countimg='<?=@count(json_decode($val2['Images']))?>' type="file" name="files[]" id="files_<?=$val2['MsfdId']?>" multiple class="form-control-file files">
                                                                    <p style="color: red;">Số lượng tối đa là <?=$maxImage?> tấm ảnh</p>
                                                                </div>

                                                                
                                                            </div>
                                                            <div class="col-md-6">
                                                                <ul class="list-group list-group-unbordered mb-3">
                                                                    <li class="list-group-item">
                                                                        <b>Kết quả</b> <a class="float-right"><?=round($val2['ResualPoint'], 1)?></a>
                                                                    </li>
                                                                    <li class="list-group-item">
                                                                        <b>Max</b> <a class="float-right"><?=$val2['MaxPoint']?></a>
                                                                    </li>
                                                                    <li class="list-group-item">
                                                                        <b>Trọng số</b> <a class="float-right"><?=$val2['PercentagePoint'].'%'?></a>
                                                                    </li>
                                                                </ul>
                                                                
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <div style="display: none;" class="alert alert-danger" id="message_<?=$val2['MsfdId']?>" role="alert">
                                                                        This is a danger alert—check it out!
                                                                    </div>
                                                                    <div class="row" id="uploaded_images_<?=$val2['MsfdId']?>">
                                                                    <?php if(!empty( $val2['Images'] )) :
                                                                        $image = json_decode( $val2['Images'] );
                                                                        if(is_array($image)):
                                                                        $dataImage = array(
                                                                            'image_current_'.$val2['MsfdId'] => $image
                                                                        );
                                                                        session()->set($dataImage);
                                                                        foreach($image as $k => $v):
                                                                        ?>
                                                                        <div class="col-sm-2">
                                                                            <a msfdid="<?=$val2['MsfdId']?>" shopid="<?=$stores['MsId']?>" onclick="del_image(this)" name_image="<?=$v?>" class="del_image" title="Xóa" class="btn btn-default"><i class="fas fa-trash-alt"></i> Xoá</a>
                                                                            <img src="<?= base_url().'/uploads/'.$v?>" class="img-responsive img-thumbnail" data-action="zoom">
                                                                        </div>
                                                                    <?php endforeach;?>
                                                                    <?php endif; endif;?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>   
                                                    <?php else:?>
                                                        <!-- <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group col-md-12">
                                                                    <?php 
                                                                        $data = array(
                                                                            'name'        => 'note_'.$val2['MsfdId'],
                                                                            'id'          => 'note_'.$val2['MsfdId'],
                                                                            'value'       => set_value('note', $val2['Note']),
                                                                            'rows'        => '2',
                                                                            'cols'        => '4',
                                                                            'class'       => 'form-control'
                                                                        );
                                                                        echo form_textarea($data);
                                                                    ?>
                                                                </div>
                                                                <div class="form-group formuploadimg_post col-md-6">
                                                                    <label>Hình ảnh</label>
                                                                    <input maximage="<?=$maxImage?>" msfdid="<?=$val2['MsfdId']?>" shopid="<?=$stores['MsId']?>" countimg='<?=@count(json_decode($val2['Images']))?>' type="file" name="files[]" id="files_<?=$val2['MsfdId']?>" multiple class="form-control-file files">
                                                                    <p style="color: red;">Số lượng tối đa là <?=$maxImage?> tấm ảnh</p>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div style="display: none;" class="alert alert-danger" id="message_<?=$val2['MsfdId']?>" role="alert">
                                                                        This is a danger alert—check it out!
                                                                    </div>
                                                                    <div class="row" id="uploaded_images_<?=$val2['MsfdId']?>">
                                                                    <?php if(!empty( $val2['Images'] )) :
                                                                        $image = json_decode( $val2['Images'] );
                                                                        if(is_array($image)):
                                                                        $dataImage = array(
                                                                            'image_current_'.$val2['MsfdId'] => $image
                                                                        );
                                                                        session()->set($dataImage);
                                                                        foreach($image as $k => $v):
                                                                        ?>
                                                                        <div class="col-sm-3">
                                                                            <a msfdid="<?=$val2['MsfdId']?>" shopid="<?=$stores['MsId']?>" onclick="del_image(this)" name_image="<?=$v?>" class="del_image" title="Xóa">x</a>
                                                                            <img src="<?= site_url().URL_SHOPS.$v?>" class="img-responsive img-thumbnail" data-action="zoom">
                                                                        </div>
                                                                    <?php endforeach;?>
                                                                    <?php endif; endif;?>
                                                                    </div>
                                                                </div> 
                                                            </div> 
                                                        </div> -->
                                                    <?php endif;?>
                                                    
                                                
                                            </div>
                                            </div>
                                        </div>
                                        
                                    <?php 
                                        endforeach;
                                        endif;
                                    ?>
                                    </div>
                                    </div>
                                    
                                </div>
                            </div>
                            
                            
                        <?php endforeach;?>
                    <?php
                endif;
            ?>
            </div>
    </div>
        </div>
        </div>
    <div class="card-footer" style="position: fixed;
    bottom: 0;
    width: 100%;
    z-index: 11111;
    background-color: aliceblue;
    margin: 0 auto;">
        <?php if($stores['Status'] <= 5):?>
            <?= form_submit('add_review', 'Lưu', 'class="btn btn-success"'); ?>
        <?php else:?>
            <a class="btn btn-success" href="<?= base_url("stores") ?>">Danh sách</a>
        <?php endif;?>

        <a class="btn btn-default" href="<?= base_url("stores/review/".$stores['MsId']) ?>">Huỷ</a>
    </div>
    <?= form_close();?>
</div>


    <!-- <div id="actions" class="row">
        <div class="col-lg-6">
            <div class="btn-group w-100">
                <span class="btn btn-success col fileinput-button dz-clickable"><i class="fas fa-plus"></i><span>Add files</span></span>
                <button type="submit" class="btn btn-primary col start"><i class="fas fa-upload"></i><span>Start upload</span></button>
                <button type="reset" class="btn btn-warning col cancel"><i class="fas fa-times-circle"></i><span>Cancel upload</span></button>
            </div>
        </div>
        <div class="col-lg-6 d-flex align-items-center">
            <div class="fileupload-process w-100">
                <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                    <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress=""></div>
                </div>
            </div>
        </div>
    </div>
    <div class="table table-striped files" id="previews">
        <div id="template" class="row mt-2">
            <div class="col-auto">
                <span class="preview"><img src="data:," alt="" data-dz-thumbnail /></span>
            </div>
            <div class="col d-flex align-items-center">
                <p class="mb-0">
                <span class="lead" data-dz-name></span>
                (<span data-dz-size></span>)
                </p>
                <strong class="error text-danger" data-dz-errormessage></strong>
            </div>
            <div class="col-4 d-flex align-items-center">
                <div class="progress progress-striped active w-100" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                    <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                </div>
            </div>
            <div class="col-auto d-flex align-items-center">
                <div class="btn-group">
                    <button class="btn btn-primary start"><i class="fas fa-upload"></i><span>Start</span></button>
                    <button data-dz-remove class="btn btn-warning cancel"><i class="fas fa-times-circle"></i><span>Cancel</span></button>
                    <button data-dz-remove class="btn btn-danger delete"><i class="fas fa-trash"></i><span>Delete</span></button>
                </div>
            </div>
        </div>
    </div> -->
   
    

    <!-- <form id="id_dropzone" 
        class="dropzone" 
        action="/stores/submitStore"
        enctype="multipart/form-data" 
        method="post">
    </form> -->


<!-- /.card -->
<?= $this->endSection() ?>