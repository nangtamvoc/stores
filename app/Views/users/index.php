<?= $this->extend('layouts/master') ?>

<?= $this->section('head') ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

<!-- Jquery Confirm -->
<link rel="stylesheet" href="<?= base_url('assets/adminlte3') ?>/plugins/jquery-confirm/jquery-confirm.css">
<link rel="stylesheet" href="<?= base_url('assets') ?>/css/main.css">
<style>
    .dt-buttons{
        float: none;
    }
</style>
<?= $this->endSection() ?>

<?= $this->section('foot') ?>
<!-- DataTables -->
<script src="<?= base_url('assets/adminlte3') ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url('assets')?>/js/custom-datatablejs.js"></script>

<!-- jquery-validation -->
<script src="<?= base_url('assets/adminlte3') ?>/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?= base_url('assets/adminlte3') ?>/plugins/jquery-validation/additional-methods.min.js"></script>
<!-- Jquery Confirm -->
<script src="<?= base_url('assets/adminlte3') ?>/plugins/jquery-confirm/jquery-confirm.js"></script>

<!-- page script -->
<script>
function pstatus(x) {
    if (x == 0) {
        return '<button type="button" class="btn btn-warning btn-sm">Không Kích hoạt</button>';
    } else if (x == 1) {
        return '<button type="button" class="btn btn-success btn-sm">Kích hoạt</button>';
    } else {
        return '<button type="button" class="btn btn-warning btn-sm">Không Kích hoạt</button>';
    }
}
$(function () {
    let getUsersApi = '<?= base_url("users") ?>';
    /*
    let userTable = $('#userTable').on('preXhr.dt', function (e, settings, data) {
        // $.LoadingOverlay("show");
    }).on('xhr.dt', function (e, settings, json, xhr) {
        // $.LoadingOverlay("hide");
    }).DataTable({
        dom: 'lBfrtip',
        scrollY: '60vh',
        deferRender: true,
        scrollCollapse: true,
        serverSide: true,
        responsive: true,
        ordering: false,
        select: {
            info: false,
            style: 'single',
        },
        search: {
            "caseInsensitive": true,
        },
        lengthMenu: [[100, 200, 400], [100, 200, 400]],
        language: Datatable_Language,
        ajax: getUsers(),
        columns: [
            {title: "Tên", width: 100, data: 'FullName'},
            {title: "Email", data: 'Email'},
            {title: "Số điện thoại", data: 'PhoneNumber'},
            {title: "Trạng thái", data: 'Status'},
        ],
        order: [[0, "desc"]],
        buttons: [
        ],
        initComplete: function (settings, json) {
            // $('#userTable_filter input').unbind();
            $('#userTable_filter input').bind('keyup', function (e) {
                console.log(e.keyCode);
                if (e.keyCode == 13) {
                    userTable.search(this.value).draw();
                }
            });
        },
    });

    function getUsers() {
        return $.fn.dataTable.pipeline({
            url: getUsersApi,
            pages: 5, // number of pages to cache,
            method: 'post',
            data: {
                
            },
        });
    }
    */
    var userTable = $('#userTable').DataTable({
        dom: 'Blfrtip',
        ajax : { url: getUsersApi, type: 'POST', "data": function ( d ) {   
            d.<?= csrf_token() ?> = "<?= csrf_hash() ?>";
        }},
        language: Datatable_Language,
        "responsive": true,
        "autoWidth": false,
        buttons: [
            {
                text: '<i class="fas fa-plus"></i> Thêm mới',
                className: 'btn btn-sm btn-success ms-1 ml-3',
                action: function (e, dt, node, config) {
                    $('#createUserModal').on('show.bs.modal', function (event) {
                        $('#createUserModal #form-user').trigger("reset");
                        var modal = $(this);
                        modal.find('.modal-title').text('Thêm người dùng');
                        // modal.find('#name').prop('readonly', false);
                        modal.find('#type').val('add');
                    });
                    $('#createUserModal').modal('show');
                },
            },
        ],
        columns: [
            { title: "Tên đăng nhập", width: 150, data: 'UserName'},
            { title: "Tên", data: 'FullName'},
            { title: "Email", data: 'Email'},
            { title: "Số điện thoại",  width: 100, data: 'PhoneNumber'},
            { title: "Trạng thái", searchable: false, orderable: false, data: 'Status', render: pstatus},
        ]
    });

    userTable.on('dblclick', 'tr', function (e, dt, type, cell, originalEvent) {
        if (!userTable.rows().data().length) return;
        let rowData = userTable.row(this).data();

        $('#createUserModal').on('show.bs.modal', function (event) {
            validate_form.resetForm();
            $('#createUserModal #form-user').trigger("reset");
            var modal = $(this);
            modal.find('.is-invalid').removeClass('is-invalid');
            modal.find('.modal-title').text('Cập nhật thông tin: ' + rowData.FullName);
            modal.find('#type').val('update');
            modal.find('#userName').val(rowData.UserName).prop('readonly', true);
            modal.find('#fullName').val(rowData.FullName);
            modal.find('#email').val(rowData.Email);
            modal.find('#phoneNumber').val(rowData.PhoneNumber);
            modal.find('#password').removeAttr('required')
        });
        $('#createUserModal').modal('show');
    });

    jQuery.validator.addMethod('valid_email', function (value, element) {
        var regex = /^[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{1,5}$/;
        return this.optional(element) || regex.test(value);
    }, 'Vui lòng nhập đúng định dạng');
    jQuery.validator.addMethod("customphone", function (value, element) {
        return this.optional(element) || /((09|03|07|08|05)+([0-9]{8})\b)/.test(value);
    }, 'Vui lòng nhập đúng định dạng số điện thoại');

    var validate_form = $('#form-user').validate({
        rules: {
            userName: {
                required: true,
            },
            password: {
                // required: true,
                minlength: 5
            },
            repassword: {
                equalTo : "#password"
            },
            fullName: {
                required: true,
            },
            email: {
                required: true,
                // email: true,
                valid_email: true
            },
            phoneNumber: {
                required: true,
                number: true,
                customphone: true
            }
        },
        messages: {
            userName: {
                required: "Nhập tên đăng nhập của bạn"
            },
            password: {
                required: "Nhập mật khẩu của bạn",
                minlength: "Mật khẩu phải lớn hơn 5 ký tự"
            },
            repassword: {
                required: "Nhập lại mật khẩu của bạn",
                equalTo: "Mật khẩu nhập lại không trùng khớp"
            },
            fullName: {
                required: "Nhập họ và tên của bạn",
            },
            email: {
                required: "Vui lòng nhập địa chỉ email",
            },
            phoneNumber: {
                required: "Vui lòng nhập số điện thoại",
                number: 'Số điện thoại phải là số'
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        submitHandler: function (form) {
            var paramArr = $(form).serializeArray();
            console.log(paramArr)
            $.post( $('#form-user').attr('action'), $.param(paramArr), [], 'json')
                    .done(function (e) {
                        console.log(e);
                        if(e.success){
                            jqueryAlert('Thông báo', e.message, 'green');
                            $('#createUserModal').modal('hide');
                            userTable.ajax.reload();
                        }else{
                            jqueryAlert('Thông báo', e.message, 'red');
                        }
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        console.log(xhr.responseText);
                    });
        }
    });

});

</script>

<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="row">
    <div class="col-12">

        <div class="card border-top border-top-3 border-success">
            <div class="card-header">
                <!-- <button class="btn btn-sm btn-success ms-1" type="button"><span><i class="fas fa-plus"></i> Thêm mới</span></button> -->
            </div>
            <div class="card-body">
            <table id="userTable" class="table table-bordered table-striped">
            </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<!-- Modal -->
<div class="modal fade" id="createUserModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Default Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php
                $attributes = ['class' => 'form-user', 'id' => 'form-user'];
                echo form_open(base_url('/users/submitUser'), $attributes);
                $data = [
                    'type'  => 'hidden',
                    'name'  => 'type',
                    'id'    => 'type',
                    'value' => 'add',
                    'class' => 'type',
                ];
                echo form_input($data);
                ?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="userName">Tên đăng nhập <label class="text-danger">(*)</label></label>
                    <?= form_input('userName', set_value('userName'), 'class="form-control" id="userName" required');?>
                </div>

                <div class="form-group">
                    <label for="userName">Mật khẩu <label class="text-danger">(*)</label></label>
                    <?= form_password([
                        'name'      => 'password',
                        'id'        => 'password',
                        'value'     => '',
                        'class'     => 'form-control',
                        'placeholder'     => 'Mật khẩu',
                        'required'  => 'required'
                    ]);?>
                </div>

                <div class="form-group">
                    <label for="userName">Nhập lại mật khẩu <label class="text-danger">(*)</label></label>
                    <?= form_password([
                        'name'      => 'repassword',
                        'id'        => 'repassword',
                        'value'     => '',
                        'class'     => 'form-control',
                        'placeholder'     => 'Nhập lại mật khẩu',
                    ]);?>
                </div>

                <div class="form-group">
                    <label for="fullName">Họ và tên <label class="text-danger">(*)</label></label>
                    <?= form_input('fullName', set_value('fullName'), 'class="form-control" id="fullName" required');?>
                </div>
                <div class="form-group">
                    <label for="email">Email <label class="text-danger">(*)</label></label>
                    <?= form_input('email', set_value('email'), 'class="form-control" id="email" required');?>
                </div>
                <div class="form-group">
                    <label for="phoneNumber">Số điện thoại <label class="text-danger">(*)</label></label>
                    <?= form_input('phoneNumber', set_value('phoneNumber'), 'class="form-control" id="phoneNumber" required');?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <?php 
                $data = [
                    'name'    => 'send',
                    'id'      => 'send',
                    'value'   => 'send',
                    'type'    => 'submit',
                    'class'   => 'btn btn-success',
                    'content' => 'Lưu'
                ];
                echo form_button($data);
                ?>
            </div>
            <?= form_close(); ?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?= $this->endSection() ?>