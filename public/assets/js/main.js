var showLoadingOverlay = true;
$(function () {

    $('#selectProvince').on('change', function (e) {
        e.preventDefault();
        var myValProvince = this.value;
        $(this).next().next().css({'display': 'none'});
        var temp = $('#selectDistrict');
        $.post("/stores/getDistricts", { provinceCode: myValProvince }, function(e) {
            if(e.success == false){
                return;
            }else{
                $('#selectWard').empty();
                temp.empty();
                temp.val();
                $.each(e.data, function(i, datav) {
                    $('<option>', {
                        value: i,
                        text: datav
                    }).html(datav).appendTo(temp);
                });
            }
        }, 'json');
    });

    $('#selectDistrict').on('change', function (e) {
        e.preventDefault();
        var myValDistrict = this.value;
        $(this).next().next().css({'display': 'none'});
        var temp = $('#selectWard');
        $.post("/stores/getWards", { districtCode: myValDistrict }, function(e) {
          if(e.success == false){
            return;
          }else{
            temp.empty();
            temp.val();
            temp.next().next().css({'display': 'none'});
            $.each(e.data, function(i, datav) {
                $('<option>', {
                    value: i,
                    text: datav
                }).html(datav).appendTo(temp);
            });
          }
        }, 'json');
    });
    $('#selectWard').on('change', function (e) { e.preventDefault(); $(this).next().next().css({'display': 'none'}); })

    $('.toggle_form').click(function () {
        $("#form").slideToggle();
        return false;
    });

    $(document).ajaxSend(function (event, jqxhr, settings) {
        if (showLoadingOverlay)
            $('.content').LoadingOverlay("show");
    });

    $(document).ajaxComplete(function (event, jqxhr, settings) {
        showLoadingOverlay = true;
        $('.content').LoadingOverlay("hide");
    });
    
})
var tabItems=document.querySelectorAll(".files");
tabItems.forEach(tabItem=>{
    tabItem.addEventListener("change",e=>{
        e.preventDefault();
        let shopid=tabItem.getAttribute("shopid")
        let countimgcurrent=tabItem.getAttribute("countimg")
        let msfdid=tabItem.getAttribute("msfdid")
        let maximage=tabItem.getAttribute("maximage")
        var files = $(tabItem)[0].files
        var countimgnew = files.length
        var error = ''
        var form_data = new FormData()
        form_data.append("shopid", shopid)
        form_data.append("msfdid", msfdid)

        // console.log(shopid)
        // console.log(countimgnew)
        console.log(files)
        // console.log(countimgcurrent)
        if ((Number(countimgnew) + Number(countimgcurrent)) > Number(maximage)) {
            error += "<p id='error'>Vượt số lượng hình ảnh cho phép</p>" + "<h4>Note</h4>" + "<span id='error_message'>Số lượng tối đa là "+ Number(maximage) +"</span><button type='button' class='close' data-dismiss='alert' aria-label='Close'> <span aria-hidden='true'>&times;</span></button>";
        } else {
            if(files.length > 0){
                for (var count = 0; count < files.length; count++) {
                    var name = files[count].name;
                    var extension = name.split('.').pop().toLowerCase();
                    if (jQuery.inArray(extension, ['png', 'jpg', 'jpeg']) == -1) {
                        error += "<p id='error'>Vui lòng chọn đúng định dạng hình ảnh</p>" + "<h4>Note</h4>" + "<span id='error_message'>Chỉ có thể: jpeg, jpg and png được sử dụng</span><button type='button' class='close' data-dismiss='alert' aria-label='Close'> <span aria-hidden='true'>&times;</span></button>"
                        
                    } else {
                        console.log(files[count])
                        form_data.append("files[]", files[count]);
                    }
                }
            }else error += "<p>Vui lòng chọn file</p>"
        }
        console.log(form_data)
        if (error == '') {
            $.ajax({
                url: "/stores/upload_image",
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    $('#uploaded_images_'+ msfdid).html('').append(data);
                    // $('#files').val('');
                    $("#message_"+ msfdid).hide().html('');
                    tabItem.setAttribute('countimg', Number(countimgnew) + Number(countimgcurrent));
                }
            })
        } else {
            $("#message_"+ msfdid).show().html(error);
            $(tabItem).val('');
            return false;
        }
    })
});

function del_image(th) {
    var shopid = $(th).attr('shopid');
    var name_img = $(th).attr('name_image');
    var msfdid = $(th).attr('msfdid')
    var countimg = $('#files_'+msfdid).attr('countimg');
    if (confirm('Bạn có chắc chắn muốn xóa tấm ảnh này?')) {
        $.ajax({
            url: "/stores/delImageSession",
            type: "POST",
            data: {shopid: shopid, name_img: name_img, msfdid: msfdid},
            beforeSend: function () {
                // $('#uploaded_images').html("<label class='text-success'>Uploading...</label>");
            },
            success: function (data) {
                $('#files_'+msfdid).attr('countimg', Number(countimg) - 1);
                $('#files_'+msfdid).val('');
                $('#uploaded_images_'+msfdid).html(data);
                $("#message_"+msfdid).hide().html('');
                
            }
        })
    }
    return false;
}

function formatFullDate(date) {
    if (date !== null) {
        return moment(date).format('DD-MM-YYYY HH:mm:ss');
    }
    return (date === null) ? '' : date;
}

function formatDate(date) {
    if (date !== null) {
        return moment(date).format('DD-MM-YYYY');
    }
    return (date === null) ? '' : date;
}

function validateNumber(evt) {
    var theEvent = evt || window.event;

    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}


function getTimeString() {
    return new Date().getTime();
}

function getDataSelectedRowDatatable(table) {
    return table.rows({selected: true}).data();
}

function getSelectedRowId(table, index = 0) {
    return table.rows({selected: true})[0][index];
}

function getRowDataById(table, rowId) {
    return table.row(rowId).data();
    // return table.rows({selected: true}).data()[index];
}

function travelerTotal(items, prop) {
    return items.reduce(function (a, b) {
        return a + b[prop];
    }, 0);
}

function jqueryAlert(title, content, type) {
    $.alert({
        animation: 'none',
        title: title,
        content: content,
        type: type,
        containerFluid: true,
        // boxWidth: '25%',
        // useBootstrap: false,
        buttons: {
            close: {
                text: 'Đóng',
                btnClass: 'btn-secondary',
                action: function () {
                }
            }
        }
    });
}

function toogleOverLay(action, selector = ''){
    if(selector != ''){
      $(selector).LoadingOverlay(action);
    }else{
      $.LoadingOverlay(action);
    }
    
  }

function validatePhone(phone) {
    const re = /((09|03|07|08|05)+([0-9]{8})\b)/;
    return re.test(phone);
}

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

